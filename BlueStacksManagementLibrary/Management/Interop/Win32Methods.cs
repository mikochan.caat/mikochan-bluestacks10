using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace BlueStacks.Management.Interop
{
    public static class Win32Methods
    {
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MINIMIZE = 0xF020;

        public const int WS_MINIMIZEBOX = 0x20000;
        public const int CS_DBLCLKS = 0x8;

        [DllImport("user32.dll", BestFitMapping = true, CharSet = CharSet.Auto)]
        public static extern int SetWindowText(IntPtr hWnd, string str);

        [DllImport("user32.dll")]
        public static extern int MoveWindow(IntPtr hWnd, int x, int y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hWnd, ref Rectangle lpRect);
    }
}
