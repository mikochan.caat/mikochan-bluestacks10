using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;

namespace BlueStacks.Management.Interop
{
    public static class SingleInstanceApp
    {
        private static readonly string[] EmptyArgs = new string[1];

        public delegate void InstanceCallback();

        public static void Run(IForm form)
        {
            new App(form).Run(EmptyArgs);
        }

        public static void Run(InstanceCallback startup, InstanceCallback nextInstance)
        {
            EnsureNotNull(startup, "Startup callback");
            EnsureNotNull(nextInstance, "Next instance callback");
            using (var mutex = new Mutex(true, GetMutexID(Assembly.GetCallingAssembly()))) {
                if (mutex.WaitOne(TimeSpan.Zero, true)) {
                    startup.Invoke();
                } else {
                    nextInstance.Invoke();
                }
            }
        }

        private static string GetMutexID(Assembly a)
        {
            var asmName = a.GetName();
            return asmName.Name + "-" + asmName.ProcessorArchitecture + "-{" +
                   ((a.GetCustomAttributes(typeof(GuidAttribute), true)[0]) as GuidAttribute).Value + "}";
        }

        private static void EnsureNotNull(object o, string objName)
        {
            if (o == null) {
                throw new ArgumentNullException(objName + " cannot be null.");
            }
        }

        public interface IForm
        {
            void HandleNextInstance();
        }

        private sealed class App : WindowsFormsApplicationBase
        {
            private readonly IForm form;

            public App(IForm form)
            {
                EnsureNotNull(form, "Form handler");
                if (!(form is Form)) {
                    throw new ArgumentException("Form must be of type " + typeof(Form).FullName);
                }
                this.IsSingleInstance = true;
                this.MainForm = form as Form;
                this.form = form;
            }

            protected override void OnStartupNextInstance(StartupNextInstanceEventArgs eventArgs)
            {
                base.OnStartupNextInstance(eventArgs);
                this.form.HandleNextInstance();
            }
        }
    }
}
