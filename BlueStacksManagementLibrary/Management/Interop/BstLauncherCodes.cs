﻿namespace BlueStacks.Management.Interop
{
    public static class BstLauncherCodes
    {
        public const int ExitOK = 0;
        public const int TaskKilled = 1;
        public const int FatalError = 64;
        public const int NoInstallDirectory = 128;
        public const int LauncherAlreadyRunning = 256;
    }
}
