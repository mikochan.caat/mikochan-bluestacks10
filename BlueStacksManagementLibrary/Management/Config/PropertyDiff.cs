using System.Collections.Generic;
using System.Linq;

namespace BlueStacks.Management.Config
{
    public sealed class PropertyDiff
    {
        private readonly Dictionary<string, object> current;
        private Dictionary<string, object> lastCommit;
        
        public bool Changed
        {
            get
            {
                return (this.current.Count > 0 && this.lastCommit == null) ||
                       (this.lastCommit != null && (this.current.Count != this.lastCommit.Count ||
                        this.current.Except(this.lastCommit).Count() > 0));
            }
        }

        public PropertyDiff()
        {
            this.current = new Dictionary<string, object>();
        }

        public void SetProperty(string prop, object value)
        {
            this.current[prop] = value;
        }

        public void RemoveProperty(string prop)
        {
            this.current.Remove(prop);
        }

        public void Commit()
        {
            if (this.lastCommit == null) {
                this.lastCommit = new Dictionary<string, object>(this.current);
            } else {
                this.lastCommit.Clear();
                foreach (string key in this.current.Keys) {
                    this.lastCommit[key] = this.current[key];
                }
            }
        }
    }
}
