﻿namespace BlueStacks.Management.Config
{
    public static class AppConstants
    {
        public static readonly string AppConfigPath = "config.xml";
        public static readonly string DisableServicesOnExit = "disableServicesOnExit";
        public static readonly string ExitBlueStacksOnWindowClose = "exitBlueStacksOnWindowClose";
        public static readonly string MinimizeToTray = "minimizeToTray";
        public static readonly string MaxProcessAttachTime = "maxProcessAttachTime";
        public static readonly string StartupSourceKey = "startupSourceKey";
        public static readonly string LogAutoClear = "logAutoClear";
        public static readonly string LogAutoClearThreshold = "logAutoClearThreshold";

        public static readonly string RegAppConfigFolder = @"\BlueStacks Control Panel\";
        public static readonly string RegAppConfigPath = RegAppConfigFolder + "regbackup.xml";
        public static readonly string RegBackupKey = "regBackupKey";

        public static readonly string UnknownPath = @".\\??\Unknown";

        public const int ProcessPollTime = 650;
        public const int MinPollTime = 5000;
        public const int DefPollTime = 60000;
        public const int MaxPollTime = 120000;
        public const int MinRequiredRAM = 768;
        public const int MaxAllocatedRAM = 850;
        public const int MinAllocatedRAM = 512;
    }
}
