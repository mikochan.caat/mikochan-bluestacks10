using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace BlueStacks.Management.Config
{
    public sealed class AppConfig
    {
        private static readonly XmlReaderSettings ReaderSettings;
        private static readonly XmlWriterSettings WriterSettings;

        private static readonly string CfgHeader = "bstconfig";
        private static readonly string CfgOption = "option";
        private static readonly string CfgName = "name";
        private static readonly string CfgValue = "value";

        static AppConfig()
        {
            ReaderSettings = new XmlReaderSettings();
            ReaderSettings.CloseInput = true;
            ReaderSettings.IgnoreComments = true;
            ReaderSettings.IgnoreWhitespace = true;
            WriterSettings = new XmlWriterSettings();
            WriterSettings.CloseOutput = true;
            WriterSettings.Indent = true;
            WriterSettings.IndentChars = "\t";
        }

        private readonly Dictionary<string, string> options;
        private readonly Dictionary<string, string> baseOptions;
        private readonly DirectoryInfo saveDir;

        public string Path
        {
            get;
            private set;
        }

        public bool LoadSuccess
        {
            get;
            private set;
        }

        public bool HasPendingChanges
        {
            get
            {
                return this.options.Count != this.baseOptions.Count ||
                       this.options.Except(this.baseOptions).Count() > 0;
            }
        }

        public AppConfig(string path)
        {
            this.options = new Dictionary<string, string>();
            this.Path = path;
            this.saveDir = new FileInfo(path).Directory;
            this.Load();
            this.baseOptions = new Dictionary<string, string>(this.options);
        }

        public void SetOption<T>(string name, T value)
        {
            if (value is string) {
                this.SetOption(name, value);
            }
            CheckIfPrimitive(typeof(T));
            this.SetOption(name, (value != null) ? value.ToString() : null);
        }

        public void SetOption(string name, string value)
        {
            this.options[name] = value ?? String.Empty;
        }

        public string GetOption(string name)
        {
            return this.GetOption(name, null);
        }

        public string GetOption(string name, string defValue)
        {
            string s;
            if (!this.options.TryGetValue(name, out s)) {
                s = defValue;
            }
            return s;
        }

        public T GetOption<T>(string name)
        {
            return this.GetOption<T>(name, default(T));
        }

        public T GetOption<T>(string name, T defValue)
        {
            Type t = typeof(T);
            if (t == typeof(string)) {
                return this.GetOption(name, defValue);
            }
            CheckIfPrimitive(t);
            string val;
            if (this.options.TryGetValue(name, out val)) {
                try {
                    return (T)Convert.ChangeType(val, t);
                } catch {
                }
            }
            return defValue;
        }

        public void RemoveOption(string name)
        {
            this.options.Remove(name);
        }

        public bool Save(bool forceSave)
        {
            bool result = true;
            if (forceSave || this.HasPendingChanges) {
                this.baseOptions.Clear();
                XmlWriter writer = null;
                try {
                    this.saveDir.Create();
                    writer = XmlWriter.Create(this.Path, WriterSettings);
                    writer.WriteStartDocument();
                    writer.WriteStartElement(CfgHeader);
                    if (this.options.Count > 0) {
                        foreach (string key in this.options.Keys) {
                            string val = this.options[key];
                            if (val != null) {
                                writer.WriteStartElement(CfgOption);
                                writer.WriteAttributeString(CfgName, key);
                                writer.WriteAttributeString(CfgValue, val);
                                writer.WriteEndElement();
                                this.baseOptions[key] = val;
                            }
                        }
                    } else {
                        writer.WriteWhitespace("");
                    }
                    writer.WriteEndElement();
                    writer.WriteWhitespace("\n");
                } catch {
                    result = false;
                } finally {
                    if (writer != null) {
                        writer.Close();
                    }
                }
            }
            return result;
        }

        private void Load()
        {
            XmlReader reader = null;
            try {
                reader = XmlReader.Create(this.Path, ReaderSettings);
                bool foundHeader = false;
                while (reader.Read()) {
                    if (reader.IsStartElement()) {
                        if (reader.Name == CfgHeader && !foundHeader) {
                            foundHeader = true;
                        } else if (foundHeader) {
                            if (reader.Name == CfgOption) {
                                try {
                                    this.SetOption(reader[CfgName], reader[CfgValue]);
                                } catch {
                                }
                            }
                        } else {
                            break;
                        }
                    }
                }
                this.LoadSuccess = true;
            } catch {
                this.LoadSuccess = false;
            } finally {
                if (reader != null) {
                    reader.Close();
                }
            }
        }

        private static void CheckIfPrimitive(Type t)
        {
            if (!t.IsPrimitive) {
                throw new ArgumentException("Type " + t.FullName + " is not supported. " +
                                            "Only primitive types and strings are supported.");
            }
        }
    }
}
