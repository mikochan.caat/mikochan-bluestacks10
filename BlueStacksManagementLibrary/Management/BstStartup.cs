using System;
using System.Threading;
using Microsoft.Win32;

namespace BlueStacks.Management
{
    public static class BstStartup
    {
        public static readonly string Entry = "BlueStacks Agent";

        private static readonly string[] SearchKeys;
        private static readonly ThreadLocal<string> FoundKey;

        static BstStartup()
        {
            if (Environment.Is64BitOperatingSystem) {
                SearchKeys = new string[] {
                    @"Software\Microsoft\Windows\CurrentVersion\Run",
                    @"Software\Microsoft\Windows\CurrentVersion\RunOnce",
                    @"Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run",
                    @"Software\Wow6432Node\Microsoft\Windows\CurrentVersion\RunOnce",
                    @"Software\Wow6432Node\Microsoft\Windows\CurrentVersion\RunOnceEx"
                };
            } else {
                SearchKeys = new string[] {
                    @"Software\Microsoft\Windows\CurrentVersion\Run",
                    @"Software\Microsoft\Windows\CurrentVersion\RunOnce",
                    @"Software\Microsoft\Windows\CurrentVersion\RunOnceEx"
                };
            }
            FoundKey = new ThreadLocal<string>();
        }

        public static string FindEntry(string exePath)
        {
            string fullKey = null;
            foreach (string key in SearchKeys) {
                object val = null;
                string sKey = Registry.LocalMachine.Name + @"\" + key;
                try {
                    val = Registry.GetValue(sKey, Entry, null);
                } catch {
                }
                if (val != null && val as string == exePath) {
                    FoundKey.Value = key;
                    fullKey = sKey;
                    break;
                }
            }
            return fullKey;
        }

        public static bool DeleteFoundEntry()
        {
            string key = FoundKey.Value;
            bool result = false;
            if (key != null) {
                RegistryKey reg = null;
                try {
                    reg = Registry.LocalMachine.OpenSubKey(key, true);
                    reg.DeleteValue(Entry);
                    FoundKey.Value = null;
                    result = true;
                } catch {
                } finally {
                    if (reg != null) {
                        reg.Dispose();
                    }
                }
            }
            return result;
        }

        public static bool RestoreEntry(string key, string exePath)
        {
            bool result = false;
            if (key != null && exePath != null) {
                try {
                    Registry.SetValue(key, Entry, exePath, RegistryValueKind.String);
                    result = true;
                } catch {
                }
            }
            return result;
        }
    }
}
