﻿using System.ComponentModel.Design;
using System.Windows.Forms;

namespace BlueStacks.Management.Utilities
{
    public static class Designer
    {
        public static bool InDesignMode(Control o)
        {
            IDesignerHost host;
            if (o.Site != null) {
                host = o.Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
                if (host != null) {
                    return host.RootComponent.Site.DesignMode;
                }
            }
            return false;
        }
    }
}
