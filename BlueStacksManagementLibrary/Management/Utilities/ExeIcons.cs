﻿using System.Drawing;

namespace BlueStacks.Management.Utilities
{
    public class ExeIcons
    {
        public static readonly Icon LauncherIcon = Properties.Resources.launcher;
        public static readonly Icon ControlPanelIcon = Properties.Resources.cpl;
    }
}
