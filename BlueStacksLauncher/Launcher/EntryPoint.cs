using System;
using System.Windows.Forms;
using BlueStacks.Management.Interop;

namespace BlueStacks.Launcher
{
    internal static class EntryPoint
    {
        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            App app = new App(args);
            SingleInstanceApp.Run(app.Run, app.NextInstance);
        }
    }
}
