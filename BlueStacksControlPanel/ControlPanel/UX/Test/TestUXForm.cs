﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UX.Test
{
    public partial class TestUXForm : Form, BlueStacks.Management.Interop.SingleInstanceApp.IForm
    {
        public TestUXForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
        }

        const int WS_MINIMIZEBOX = 0x20000;
        const int WS_SYSMENU = 0x80000;
        const int CS_DBLCLKS = 0x8;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style |= WS_MINIMIZEBOX | WS_SYSMENU;
                cp.ClassStyle |= CS_DBLCLKS;
                return cp;
            }
        }

        public void HandleNextInstance()
        {

        }

        private void TestUXForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized) {
                this.WindowState = FormWindowState.Normal;
            }
        }
    }
}
