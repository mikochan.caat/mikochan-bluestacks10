using System;
using System.Windows.Forms;
using BlueStacks.ControlPanel.UI;
using BlueStacks.Management.Interop;

namespace BlueStacks.ControlPanel
{
    internal static class EntryPoint
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SingleInstanceApp.Run(new BstCplForm());
        }
    }
}
