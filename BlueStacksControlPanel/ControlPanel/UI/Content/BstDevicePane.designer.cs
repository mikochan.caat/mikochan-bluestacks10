﻿namespace BlueStacks.ControlPanel.UI.Content
{
    partial class BstDevicePane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.prgMemAvailable = new System.Windows.Forms.ProgressBar();
            this.txtTabletHeight = new BlueStacks.ControlPanel.UI.Controls.BstNumTextBox();
            this.txtWindowHeight = new BlueStacks.ControlPanel.UI.Controls.BstNumTextBox();
            this.txtTabletWidth = new BlueStacks.ControlPanel.UI.Controls.BstNumTextBox();
            this.txtWindowWidth = new BlueStacks.ControlPanel.UI.Controls.BstNumTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkFullscreen = new System.Windows.Forms.CheckBox();
            this.prgMemAllocated = new System.Windows.Forms.ProgressBar();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtAllocatedRAM = new BlueStacks.ControlPanel.UI.Controls.BstNumTextBox();
            this.lblAllocMB = new System.Windows.Forms.Label();
            this.lblAllocRAM = new System.Windows.Forms.Label();
            this.lblMemAvailable = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblNotEnoughMem = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblWarnTablet = new System.Windows.Forms.Label();
            this.lblWarnWindow = new System.Windows.Forms.Label();
            this.lblWarnRAM = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(326, 233);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 32);
            this.btnSave.TabIndex = 7;
            this.btnSave.Tag = "Saves your options. Takes effect the next time you run the launcher.";
            this.btnSave.Text = "Save Settings";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // prgMemAvailable
            // 
            this.prgMemAvailable.Location = new System.Drawing.Point(101, 158);
            this.prgMemAvailable.Name = "prgMemAvailable";
            this.prgMemAvailable.Size = new System.Drawing.Size(291, 10);
            this.prgMemAvailable.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.prgMemAvailable.TabIndex = 4;
            this.prgMemAvailable.Tag = "Maximum amount of memory that BlueStacks can possibly receive.";
            // 
            // txtTabletHeight
            // 
            this.txtTabletHeight.AcceptButton = this.btnSave;
            this.txtTabletHeight.AcceptsReturn = true;
            this.txtTabletHeight.Location = new System.Drawing.Point(288, 77);
            this.txtTabletHeight.MaxValue = 1080;
            this.txtTabletHeight.MinValue = 240;
            this.txtTabletHeight.Name = "txtTabletHeight";
            this.txtTabletHeight.PropertyName = "Tablet Height";
            this.txtTabletHeight.Size = new System.Drawing.Size(72, 22);
            this.txtTabletHeight.TabIndex = 4;
            this.txtTabletHeight.Tag = "Sets the height of the Android Device. This is the physical height of the device." +
                "";
            this.txtTabletHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTabletHeight.Value = 240;
            // 
            // txtWindowHeight
            // 
            this.txtWindowHeight.AcceptButton = this.btnSave;
            this.txtWindowHeight.AcceptsReturn = true;
            this.txtWindowHeight.Location = new System.Drawing.Point(84, 77);
            this.txtWindowHeight.MaxValue = 240;
            this.txtWindowHeight.MinValue = 240;
            this.txtWindowHeight.Name = "txtWindowHeight";
            this.txtWindowHeight.PropertyName = "Window Height";
            this.txtWindowHeight.Size = new System.Drawing.Size(72, 22);
            this.txtWindowHeight.TabIndex = 2;
            this.txtWindowHeight.Tag = "Sets the height of the BlueStacks emulator window. This only scales the Android d" +
                "evice.";
            this.txtWindowHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWindowHeight.Value = 240;
            // 
            // txtTabletWidth
            // 
            this.txtTabletWidth.AcceptButton = this.btnSave;
            this.txtTabletWidth.AcceptsReturn = true;
            this.txtTabletWidth.Location = new System.Drawing.Point(288, 49);
            this.txtTabletWidth.MaxValue = 1920;
            this.txtTabletWidth.MinValue = 320;
            this.txtTabletWidth.Name = "txtTabletWidth";
            this.txtTabletWidth.PropertyName = "Tablet Width";
            this.txtTabletWidth.Size = new System.Drawing.Size(72, 22);
            this.txtTabletWidth.TabIndex = 3;
            this.txtTabletWidth.Tag = "Sets the width of the Android Device. This is the physical width of the device.";
            this.txtTabletWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTabletWidth.Value = 320;
            // 
            // txtWindowWidth
            // 
            this.txtWindowWidth.AcceptButton = this.btnSave;
            this.txtWindowWidth.AcceptsReturn = true;
            this.txtWindowWidth.Location = new System.Drawing.Point(84, 49);
            this.txtWindowWidth.MaxValue = 320;
            this.txtWindowWidth.MinValue = 320;
            this.txtWindowWidth.Name = "txtWindowWidth";
            this.txtWindowWidth.PropertyName = "Window Width";
            this.txtWindowWidth.Size = new System.Drawing.Size(72, 22);
            this.txtWindowWidth.TabIndex = 1;
            this.txtWindowWidth.Tag = "Sets the width of the BlueStacks emulator window. This only scales the Android de" +
                "vice.";
            this.txtWindowWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtWindowWidth.Value = 320;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.Location = new System.Drawing.Point(240, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Height:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(36, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Height:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Location = new System.Drawing.Point(362, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "px";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(158, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "px";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Location = new System.Drawing.Point(362, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "px";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Location = new System.Drawing.Point(158, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "px";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Location = new System.Drawing.Point(240, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Width:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(219, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Tablet:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(36, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Width:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(15, 138);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Memory:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Window:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "BlueStacks Android Device Settings:";
            // 
            // chkFullscreen
            // 
            this.chkFullscreen.AutoSize = true;
            this.chkFullscreen.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chkFullscreen.Location = new System.Drawing.Point(84, 105);
            this.chkFullscreen.Name = "chkFullscreen";
            this.chkFullscreen.Size = new System.Drawing.Size(148, 17);
            this.chkFullscreen.TabIndex = 5;
            this.chkFullscreen.Tag = "When checked, BlueStacks will start in fullscreen mode.";
            this.chkFullscreen.Text = "Start in fullscreen mode";
            this.chkFullscreen.UseVisualStyleBackColor = true;
            // 
            // prgMemAllocated
            // 
            this.prgMemAllocated.Location = new System.Drawing.Point(101, 176);
            this.prgMemAllocated.Name = "prgMemAllocated";
            this.prgMemAllocated.Size = new System.Drawing.Size(291, 10);
            this.prgMemAllocated.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.prgMemAllocated.TabIndex = 4;
            this.prgMemAllocated.Tag = "Amount of memory allocated to BlueStacks as a percentage of the Available memory." +
                "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(36, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Available:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label14.Location = new System.Drawing.Point(36, 174);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Allocated:";
            // 
            // txtAllocatedRAM
            // 
            this.txtAllocatedRAM.AcceptButton = this.btnSave;
            this.txtAllocatedRAM.AcceptsReturn = true;
            this.txtAllocatedRAM.Location = new System.Drawing.Point(135, 194);
            this.txtAllocatedRAM.MaxValue = 0;
            this.txtAllocatedRAM.MinValue = 0;
            this.txtAllocatedRAM.Name = "txtAllocatedRAM";
            this.txtAllocatedRAM.PropertyName = "Allocated RAM";
            this.txtAllocatedRAM.Size = new System.Drawing.Size(56, 22);
            this.txtAllocatedRAM.TabIndex = 6;
            this.txtAllocatedRAM.Tag = "Sets the amount of RAM given to BlueStacks. BlueStacks can only handle up to ";
            this.txtAllocatedRAM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAllocatedRAM.Value = 0;
            // 
            // lblAllocMB
            // 
            this.lblAllocMB.AutoSize = true;
            this.lblAllocMB.BackColor = System.Drawing.Color.Transparent;
            this.lblAllocMB.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAllocMB.Location = new System.Drawing.Point(194, 199);
            this.lblAllocMB.Name = "lblAllocMB";
            this.lblAllocMB.Size = new System.Drawing.Size(24, 13);
            this.lblAllocMB.TabIndex = 1;
            this.lblAllocMB.Text = "MB";
            // 
            // lblAllocRAM
            // 
            this.lblAllocRAM.AutoSize = true;
            this.lblAllocRAM.BackColor = System.Drawing.Color.Transparent;
            this.lblAllocRAM.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAllocRAM.Location = new System.Drawing.Point(97, 199);
            this.lblAllocRAM.Name = "lblAllocRAM";
            this.lblAllocRAM.Size = new System.Drawing.Size(34, 13);
            this.lblAllocRAM.TabIndex = 1;
            this.lblAllocRAM.Text = "RAM:";
            // 
            // lblMemAvailable
            // 
            this.lblMemAvailable.AutoSize = true;
            this.lblMemAvailable.BackColor = System.Drawing.Color.Transparent;
            this.lblMemAvailable.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblMemAvailable.Location = new System.Drawing.Point(395, 157);
            this.lblMemAvailable.Name = "lblMemAvailable";
            this.lblMemAvailable.Size = new System.Drawing.Size(25, 13);
            this.lblMemAvailable.TabIndex = 1;
            this.lblMemAvailable.Text = "850";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label18.Location = new System.Drawing.Point(418, 157);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "MB";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblNotEnoughMem);
            this.panel1.Location = new System.Drawing.Point(98, 173);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(355, 47);
            this.panel1.TabIndex = 9;
            // 
            // lblNotEnoughMem
            // 
            this.lblNotEnoughMem.AutoSize = true;
            this.lblNotEnoughMem.Location = new System.Drawing.Point(0, 1);
            this.lblNotEnoughMem.Name = "lblNotEnoughMem";
            this.lblNotEnoughMem.Size = new System.Drawing.Size(116, 13);
            this.lblNotEnoughMem.TabIndex = 0;
            this.lblNotEnoughMem.Text = "Not enough memory.";
            this.lblNotEnoughMem.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.lblWarnTablet);
            this.flowLayoutPanel1.Controls.Add(this.lblWarnWindow);
            this.flowLayoutPanel1.Controls.Add(this.lblWarnRAM);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.ForeColor = System.Drawing.Color.LightCoral;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1, 226);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(333, 39);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // lblWarnTablet
            // 
            this.lblWarnTablet.AutoSize = true;
            this.lblWarnTablet.Location = new System.Drawing.Point(3, 26);
            this.lblWarnTablet.Name = "lblWarnTablet";
            this.lblWarnTablet.Size = new System.Drawing.Size(303, 13);
            this.lblWarnTablet.TabIndex = 1;
            this.lblWarnTablet.Text = "WARNING: Tablet width or height is higher than 1920x1080.";
            this.lblWarnTablet.Visible = false;
            // 
            // lblWarnWindow
            // 
            this.lblWarnWindow.AutoSize = true;
            this.lblWarnWindow.Location = new System.Drawing.Point(3, 13);
            this.lblWarnWindow.Name = "lblWarnWindow";
            this.lblWarnWindow.Size = new System.Drawing.Size(314, 13);
            this.lblWarnWindow.TabIndex = 2;
            this.lblWarnWindow.Text = "WARNING: Current window size will not fully fit to the screen.";
            this.lblWarnWindow.Visible = false;
            // 
            // lblWarnRAM
            // 
            this.lblWarnRAM.AutoSize = true;
            this.lblWarnRAM.Location = new System.Drawing.Point(3, 0);
            this.lblWarnRAM.Name = "lblWarnRAM";
            this.lblWarnRAM.Size = new System.Drawing.Size(309, 13);
            this.lblWarnRAM.TabIndex = 3;
            this.lblWarnRAM.Text = "WARNING: RAM amount is too high or may cause instability.";
            this.lblWarnRAM.Visible = false;
            // 
            // BstDevicePane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label18);
            this.Controls.Add(this.lblMemAvailable);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.prgMemAllocated);
            this.Controls.Add(this.prgMemAvailable);
            this.Controls.Add(this.chkFullscreen);
            this.Controls.Add(this.txtTabletHeight);
            this.Controls.Add(this.txtAllocatedRAM);
            this.Controls.Add(this.txtWindowHeight);
            this.Controls.Add(this.txtTabletWidth);
            this.Controls.Add(this.txtWindowWidth);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblAllocRAM);
            this.Controls.Add(this.lblAllocMB);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "BstDevicePane";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Controls.BstNumTextBox txtWindowWidth;
        private Controls.BstNumTextBox txtWindowHeight;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Controls.BstNumTextBox txtTabletWidth;
        private Controls.BstNumTextBox txtTabletHeight;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ProgressBar prgMemAvailable;
        private Controls.BstButton btnSave;
        private System.Windows.Forms.CheckBox chkFullscreen;
        private System.Windows.Forms.ProgressBar prgMemAllocated;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Controls.BstNumTextBox txtAllocatedRAM;
        private System.Windows.Forms.Label lblAllocMB;
        private System.Windows.Forms.Label lblAllocRAM;
        private System.Windows.Forms.Label lblMemAvailable;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblNotEnoughMem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblWarnTablet;
        private System.Windows.Forms.Label lblWarnWindow;
        private System.Windows.Forms.Label lblWarnRAM;
    }
}
