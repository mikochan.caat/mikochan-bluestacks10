﻿using System;

namespace BlueStacks.ControlPanel.UI.Content
{
    internal sealed partial class BstAboutPane : BstContentPane
    {
        public BstAboutPane() : base()
        {
            this.InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.ControlPanel.RegisterStatusDescription(this.lblNameJP);
        }
    }
}
