﻿namespace BlueStacks.ControlPanel.UI.Content
{
    partial class BstMiscPane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCheckStartup = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.chkPatchUpdater = new System.Windows.Forms.CheckBox();
            this.chkDisableLogRotator = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBackupEntry = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblLogDir = new System.Windows.Forms.Label();
            this.lnkOpenLogFolder = new System.Windows.Forms.LinkLabel();
            this.lnkClearLogs = new System.Windows.Forms.LinkLabel();
            this.chkLogAutoClear = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAutoClearSize = new BlueStacks.ControlPanel.UI.Controls.BstNumTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlLogSizeThreshold = new System.Windows.Forms.Panel();
            this.btnSave = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlLogSizeThreshold.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCheckStartup
            // 
            this.btnCheckStartup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnCheckStartup.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnCheckStartup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCheckStartup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btnCheckStartup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckStartup.ForeColor = System.Drawing.Color.White;
            this.btnCheckStartup.Location = new System.Drawing.Point(28, 125);
            this.btnCheckStartup.Name = "btnCheckStartup";
            this.btnCheckStartup.Size = new System.Drawing.Size(162, 23);
            this.btnCheckStartup.TabIndex = 3;
            this.btnCheckStartup.Tag = "Checks for the startup entry of BlueStacks. Prompts the user to remove it or rest" +
                "ore from backup.";
            this.btnCheckStartup.Text = "Check Startup Entry";
            this.btnCheckStartup.UseVisualStyleBackColor = false;
            this.btnCheckStartup.Click += new System.EventHandler(this.btnCheckStartup_Click);
            // 
            // chkPatchUpdater
            // 
            this.chkPatchUpdater.AutoSize = true;
            this.chkPatchUpdater.Location = new System.Drawing.Point(28, 72);
            this.chkPatchUpdater.Name = "chkPatchUpdater";
            this.chkPatchUpdater.Size = new System.Drawing.Size(252, 17);
            this.chkPatchUpdater.TabIndex = 2;
            this.chkPatchUpdater.Tag = "When patched, the BlueStacks updater will only run for a short time and then quit" +
                ".";
            this.chkPatchUpdater.Text = "Patch BlueStacks Updater service executable";
            this.chkPatchUpdater.UseVisualStyleBackColor = true;
            // 
            // chkDisableLogRotator
            // 
            this.chkDisableLogRotator.AutoSize = true;
            this.chkDisableLogRotator.Location = new System.Drawing.Point(28, 50);
            this.chkDisableLogRotator.Name = "chkDisableLogRotator";
            this.chkDisableLogRotator.Size = new System.Drawing.Size(223, 17);
            this.chkDisableLogRotator.TabIndex = 1;
            this.chkDisableLogRotator.Tag = "Disables BlueStacks Log Rotator Service. BlueStacks does not re-enable this servi" +
                "ce.";
            this.chkDisableLogRotator.Text = "Disable BlueStacks Log Rotator service";
            this.chkDisableLogRotator.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(15, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Startup-related:";
            // 
            // lblBackupEntry
            // 
            this.lblBackupEntry.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBackupEntry.AutoEllipsis = true;
            this.lblBackupEntry.Location = new System.Drawing.Point(117, 155);
            this.lblBackupEntry.Name = "lblBackupEntry";
            this.lblBackupEntry.Size = new System.Drawing.Size(336, 13);
            this.lblBackupEntry.TabIndex = 1;
            this.lblBackupEntry.Tag = "No backup startup entry was found.";
            this.lblBackupEntry.Text = "None";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Backup Entry:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Service-related:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Other beneficial tweaks to BlueStacks:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(15, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Log-related:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Log Directory:";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this.lblLogDir);
            this.flowLayoutPanel1.Controls.Add(this.lnkOpenLogFolder);
            this.flowLayoutPanel1.Controls.Add(this.lnkClearLogs);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(102, 202);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(173, 13);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // lblLogDir
            // 
            this.lblLogDir.AutoSize = true;
            this.lblLogDir.Location = new System.Drawing.Point(3, 0);
            this.lblLogDir.MaximumSize = new System.Drawing.Size(230, 13);
            this.lblLogDir.Name = "lblLogDir";
            this.lblLogDir.Size = new System.Drawing.Size(59, 13);
            this.lblLogDir.TabIndex = 1;
            this.lblLogDir.Tag = "This is where BlueStacks stores its logs.";
            this.lblLogDir.Text = "%LogDir%";
            // 
            // lnkOpenLogFolder
            // 
            this.lnkOpenLogFolder.ActiveLinkColor = System.Drawing.Color.Silver;
            this.lnkOpenLogFolder.AutoSize = true;
            this.lnkOpenLogFolder.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lnkOpenLogFolder.LinkColor = System.Drawing.Color.WhiteSmoke;
            this.lnkOpenLogFolder.Location = new System.Drawing.Point(68, 0);
            this.lnkOpenLogFolder.Name = "lnkOpenLogFolder";
            this.lnkOpenLogFolder.Size = new System.Drawing.Size(36, 13);
            this.lnkOpenLogFolder.TabIndex = 4;
            this.lnkOpenLogFolder.TabStop = true;
            this.lnkOpenLogFolder.Tag = "Opens the folder.";
            this.lnkOpenLogFolder.Text = "Open";
            this.lnkOpenLogFolder.VisitedLinkColor = System.Drawing.Color.WhiteSmoke;
            this.lnkOpenLogFolder.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LnkOpenLogFolder_LinkClicked);
            // 
            // lnkClearLogs
            // 
            this.lnkClearLogs.ActiveLinkColor = System.Drawing.Color.Silver;
            this.lnkClearLogs.AutoSize = true;
            this.lnkClearLogs.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lnkClearLogs.LinkColor = System.Drawing.Color.WhiteSmoke;
            this.lnkClearLogs.Location = new System.Drawing.Point(110, 0);
            this.lnkClearLogs.Name = "lnkClearLogs";
            this.lnkClearLogs.Size = new System.Drawing.Size(60, 13);
            this.lnkClearLogs.TabIndex = 5;
            this.lnkClearLogs.TabStop = true;
            this.lnkClearLogs.Tag = "Clears all logs stored inside the folder.";
            this.lnkClearLogs.Text = "Clear Logs";
            this.lnkClearLogs.VisitedLinkColor = System.Drawing.Color.WhiteSmoke;
            this.lnkClearLogs.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LnkClearLogs_LinkClicked);
            // 
            // chkLogAutoClear
            // 
            this.chkLogAutoClear.AutoSize = true;
            this.chkLogAutoClear.Location = new System.Drawing.Point(108, 221);
            this.chkLogAutoClear.Name = "chkLogAutoClear";
            this.chkLogAutoClear.Size = new System.Drawing.Size(159, 17);
            this.chkLogAutoClear.TabIndex = 6;
            this.chkLogAutoClear.Tag = "When enabled, the launcher, upon exit, clears all log files when the log folder r" +
                "eaches a certain size.";
            this.chkLogAutoClear.Text = "Enable automatic clearing";
            this.chkLogAutoClear.UseVisualStyleBackColor = true;
            this.chkLogAutoClear.CheckedChanged += new System.EventHandler(this.ChkLogAutoClear_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-3, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "When size reaches";
            // 
            // txtAutoClearSize
            // 
            this.txtAutoClearSize.AcceptButton = this.btnSave;
            this.txtAutoClearSize.AcceptsReturn = true;
            this.txtAutoClearSize.Location = new System.Drawing.Point(101, -1);
            this.txtAutoClearSize.MaxValue = 1024;
            this.txtAutoClearSize.MinValue = 0;
            this.txtAutoClearSize.Name = "txtAutoClearSize";
            this.txtAutoClearSize.PropertyName = "Log Size Threshold";
            this.txtAutoClearSize.Size = new System.Drawing.Size(58, 22);
            this.txtAutoClearSize.TabIndex = 7;
            this.txtAutoClearSize.Tag = "The log size threshold. A value of 0 means that the logs are always cleared. Min:" +
                " 0 MB Max: 1024 MB.";
            this.txtAutoClearSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAutoClearSize.Value = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(162, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "MB";
            // 
            // pnlLogSizeThreshold
            // 
            this.pnlLogSizeThreshold.Controls.Add(this.txtAutoClearSize);
            this.pnlLogSizeThreshold.Controls.Add(this.label9);
            this.pnlLogSizeThreshold.Controls.Add(this.label10);
            this.pnlLogSizeThreshold.Enabled = false;
            this.pnlLogSizeThreshold.Location = new System.Drawing.Point(128, 241);
            this.pnlLogSizeThreshold.Name = "pnlLogSizeThreshold";
            this.pnlLogSizeThreshold.Size = new System.Drawing.Size(200, 24);
            this.pnlLogSizeThreshold.TabIndex = 6;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(326, 233);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 32);
            this.btnSave.TabIndex = 7;
            this.btnSave.Tag = "Saves your options. Takes effect the next time you run the launcher.";
            this.btnSave.Text = "Save Settings";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // BstMiscPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.btnCheckStartup);
            this.Controls.Add(this.chkLogAutoClear);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkPatchUpdater);
            this.Controls.Add(this.chkDisableLogRotator);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblBackupEntry);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlLogSizeThreshold);
            this.Name = "BstMiscPane";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.pnlLogSizeThreshold.ResumeLayout(false);
            this.pnlLogSizeThreshold.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkDisableLogRotator;
        private System.Windows.Forms.CheckBox chkPatchUpdater;
        private System.Windows.Forms.Label label3;
        private Controls.BstButton btnCheckStartup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblBackupEntry;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblLogDir;
        private System.Windows.Forms.LinkLabel lnkOpenLogFolder;
        private System.Windows.Forms.LinkLabel lnkClearLogs;
        private System.Windows.Forms.CheckBox chkLogAutoClear;
        private System.Windows.Forms.Label label9;
        private Controls.BstNumTextBox txtAutoClearSize;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel pnlLogSizeThreshold;
        private Controls.BstButton btnSave;
    }
}
