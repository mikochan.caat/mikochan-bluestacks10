﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI.Content
{
    internal sealed partial class BstFoldersPane : BstContentPane
    {
        public BstFoldersPane() : base()
        {
            this.InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.ControlPanel.RegisterStatusDescription(this.treeFolders);
            this.ControlPanel.RegisterStatusDescription(this.txtFolderName);
            this.ControlPanel.RegisterStatusDescription(this.txtFolderPath);
            this.ControlPanel.RegisterStatusDescription(this.chkWritable);
            this.ControlPanel.RegisterStatusDescription(this.lnkUndo);
            this.ControlPanel.RegisterStatusDescription(this.btnAdd);
            this.ControlPanel.RegisterStatusDescription(this.btnRemove);
            this.ControlPanel.RegisterStatusDescription(this.btnSave);
        }
    }
}
