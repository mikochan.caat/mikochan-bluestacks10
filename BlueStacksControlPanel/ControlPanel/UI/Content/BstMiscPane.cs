using System;
using System.ComponentModel;
using System.IO;
using System.ServiceProcess;
using System.Windows.Forms;
using BlueStacks.Management;
using BlueStacks.Management.Config;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI.Content
{
    internal sealed partial class BstMiscPane : BstContentPane
    {
        private AppConfig regAppConfig;
        private BstService logService;
        private BstService updateService;

        private bool blockSvcCheckBoxEvent;

        public BstMiscPane() : base()
        {
            this.InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.regAppConfig = new AppConfig(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + AppConstants.RegAppConfigPath);
            this.ControlPanel.RegisterStatusDescription(this.chkDisableLogRotator);
            this.ControlPanel.RegisterStatusDescription(this.chkPatchUpdater);
            this.ControlPanel.RegisterStatusDescription(this.chkLogAutoClear);
            this.ControlPanel.RegisterStatusDescription(this.btnCheckStartup);
            this.ControlPanel.RegisterStatusDescription(this.lblBackupEntry);
            this.ControlPanel.RegisterStatusDescription(this.lblLogDir);
            this.ControlPanel.RegisterStatusDescription(this.lnkOpenLogFolder);
            this.ControlPanel.RegisterStatusDescription(this.lnkClearLogs);
            this.ControlPanel.RegisterStatusDescription(this.txtAutoClearSize);
            this.ControlPanel.RegisterStatusDescription(this.btnSave);
            this.ControlPanel.RegisterSaveButton(this.btnSave, this, this.Save_Validate, this.Save_Start, null);
            this.logService = new BstService(BstService.LogRotator);
            this.updateService = new BstService(BstService.Updater);
            this.InitCheckBoxes();
            this.lblLogDir.Text = this.ControlPanel.BstProperties.DataLogsPath ?? AppConstants.UnknownPath;
            var cfg = this.ControlPanel.AppConfig;
            this.chkLogAutoClear.Checked = cfg.GetOption<bool>(AppConstants.LogAutoClear);
            this.txtAutoClearSize.Value = MathEx.Clamp(cfg.GetOption<int>(AppConstants.LogAutoClearThreshold, 0), this.txtAutoClearSize.MinValue, this.txtAutoClearSize.MaxValue);
            string backup = this.regAppConfig.GetOption(AppConstants.RegBackupKey);
            if (backup != null) {
                this.lblBackupEntry.Tag = this.lblBackupEntry.Text = backup;
            }
        }

        public override void OnApplicationClosing()
        {
            if (this.txtAutoClearSize.ValidateInput(false)) {
                this.ControlPanel.AppConfig.SetOption<int>(AppConstants.LogAutoClearThreshold, this.txtAutoClearSize.Value);
            }
        }

        #region Disposer
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (this.components != null) {
                    this.components.Dispose();
                }
                if (this.logService != null) {
                    this.logService.Dispose();
                }
                if (this.updateService != null) {
                    this.updateService.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion

        private void InitCheckBoxes()
        {
            var startMode = this.logService.StartMode;
            if (startMode != BstService.StartType.Unknown) {
                this.chkDisableLogRotator.Checked = startMode == BstService.StartType.Disabled;
                this.chkDisableLogRotator.CheckedChanged += this.ServiceCheckBox_CheckChanged;
            } else {
                this.chkDisableLogRotator.Enabled = false;
            }
            string pathName = this.updateService.PathName;
            if (this.updateService.StartMode != BstService.StartType.Unknown && pathName != null) {
                this.chkPatchUpdater.Checked = pathName == BstProperties.PatchedUpdaterExePath;
                this.chkPatchUpdater.CheckedChanged += this.ServiceCheckBox_CheckChanged;
            } else {
                this.chkPatchUpdater.Enabled = false;
            }
        }

        #region Service Handler
        private void ServiceCheckBox_CheckChanged(object sender, EventArgs e)
        {
            if (!this.blockSvcCheckBoxEvent) {
                this.blockSvcCheckBoxEvent = true;
                var chk = sender as CheckBox;
                BstService svc = null;
                string name;
                if (chk == this.chkDisableLogRotator) {
                    svc = this.logService;
                    name = BstService.LogRotatorName;
                    try {
                        svc.StartMode = (chk.Checked) ? BstService.StartType.Disabled : BstService.StartType.Automatic;
                        if (chk.Checked) {
                            if (svc.Status == ServiceControllerStatus.Running) {
                                svc.Stop();
                            }
                        }
                    } catch (Win32Exception) {
                        Dialogs.WarnOK(this, "The service has been disabled but it is still running and " +
                                       "could not be stopped.", "Service Error");
                    } catch {
                        chk.Checked = !chk.Checked;
                        Dialogs.ErrorOK(this, "An unexpected error has occured.\n" +
                                        "The " + name + " cannot be " +
                                        ((chk.Checked) ? "disabled" : "enabled") + ".", "Service Error");
                    }
                } else {
                    svc = this.updateService;
                    name = BstService.UpdaterName;
                    try {
                        svc.PathName = (chk.Checked) ? BstProperties.PatchedUpdaterExePath : this.ControlPanel.BstProperties.HDUpdaterPath;
                    } catch {
                        chk.Checked = !chk.Checked;
                        Dialogs.ErrorOK(this, "An unexpected error has occured.\n" +
                                        "The " + name + "'s executable path could not be changed.",
                                        "Service Error");
                    }
                }
                this.blockSvcCheckBoxEvent = false;
            }
        }
        #endregion

        #region Startup Entry Handler
        private void btnCheckStartup_Click(object sender, EventArgs e)
        {
            string backup = this.regAppConfig.GetOption(AppConstants.RegBackupKey);
            backup = (backup != null) ? backup.Trim() : null;
            backup = (backup == String.Empty) ? null : backup;
            string foundKey = BstStartup.FindEntry(this.ControlPanel.BstProperties.HDAgentPath);
            string opMsg = null;
            string lastOp = null;
            bool infoBox = false;
            if (foundKey != null) {
                bool remove = true;
                if (backup != null) {
                    remove = Dialogs.WarnYesNo(this, "A startup entry for BlueStacks was found " +
                                               "but there is an entry stored in backup.\n\n" +
                                               "Are you sure you want to remove the found entry?\n" +
                                               "WARNING: The entry stored in backup will be deleted " +
                                               "forever if you continue.", "Startup Entry Conflict",
                                               Dialogs.Choice.Choice2);
                }
                if (remove) {
                    if (Dialogs.QuestionYesNo(this, "A startup entry for BlueStacks was found at key:\n" +
                                              foundKey + "\n\nWould you like to remove it?",
                                              "Startup Entry Found", Dialogs.Choice.Choice2)) {
                        if (BstStartup.DeleteFoundEntry()) {
                            this.regAppConfig.SetOption(AppConstants.RegBackupKey, foundKey);
                            this.SaveRegAppConfig();
                            this.lblBackupEntry.Tag = this.lblBackupEntry.Text = foundKey;
                            infoBox = true;
                            opMsg = "The found startup entry was successfully removed.";
                            lastOp = "Startup Entry Removal Success";
                        } else {
                            infoBox = false;
                            opMsg = "The found startup entry could not be removed.";
                            lastOp = "Startup Entry Removal Failed";
                        }
                    } else {
                        infoBox = true;
                        opMsg = "No changes were made to your startup entry list.";
                        lastOp = "Startup Entry Remove Cancelled";
                    }
                } else {
                    infoBox = true;
                    opMsg = "The found startup entry was not removed due to a possible conflict.";
                    lastOp = "Startup Entry Conflict";
                }
            } else if (backup != null) {
                if (Dialogs.QuestionYesNo(this, "No startup entry for BlueStacks was found.\n\n" +
                                          "Would you like to restore from backup, the entry at:\n" +
                                          backup + "?", "Startup Entry Restore",
                                          Dialogs.Choice.Choice2)) {
                    if (BstStartup.RestoreEntry(backup, this.ControlPanel.BstProperties.HDAgentPath)) {
                        this.regAppConfig.RemoveOption(AppConstants.RegBackupKey);
                        this.SaveRegAppConfig();
                        this.lblBackupEntry.Text = "None";
                        this.lblBackupEntry.Tag = "No backup startup entry was found";
                        infoBox = true;
                        opMsg = "The startup entry was successfully restored from backup.\n\n" +
                                "Key: " + backup + "\n\nName: " + BstStartup.Entry + "\nValue: " +
                                this.ControlPanel.BstProperties.HDAgentPath;
                        lastOp = "Startup Entry Restore Success";
                    } else {
                        infoBox = false;
                        opMsg = "The startup entry could not be restored from backup.";
                        lastOp = "Startup Entry Restore Failed";
                    }
                } else {
                    infoBox = true;
                    opMsg = "No changes were made to your startup entry list.";
                    lastOp = "Startup Entry Restore Cancelled";
                }
            } else {
                infoBox = true;
                opMsg = "No startup entry was found and there was no backup to restore from.";
                lastOp = "Startup Entry Check Complete";
            }
            if (infoBox) {
                Dialogs.InfoOK(this, opMsg, lastOp);
            } else {
                Dialogs.ErrorOK(this, opMsg, lastOp);
            }
        }

        private void SaveRegAppConfig()
        {
            if (!this.regAppConfig.Save(true)) {
                Dialogs.ErrorOK(this, "Could not create the registry backup folder.\n\n" +
                                "You may lose the reference to the backup startup entry " +
                                "upon exit.", "Folder Creation Failed");
            }
        }
        #endregion

        #region Log File Handler
        private void LnkOpenLogFolder_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (!Computer.OpenFolder(this.lblLogDir.Text)) {
                Dialogs.ErrorOK(this, "There was an error in opening the folder.\nThis could be " +
                                "because the folder was deleted or is made inaccessible.",
                                "Folder Open Error");
            }
        }

        private void LnkClearLogs_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string[] paths = this.ControlPanel.BstProperties.GetDataLogFilePaths();
            if (paths != null) {
                if (Dialogs.QuestionYesNo(this, "Are you sure you want to clear all BlueStacks " +
                                          "log files?", "Confirm Delete", Dialogs.Choice.Choice2)) {
                    string err = Computer.DeleteFiles(paths);
                    if (err == null) {
                        Dialogs.InfoOK(this, "All log files have been cleared.", "Clear Log Success");
                    } else {
                        Dialogs.WarnOK(this, "Not all log files have been cleared.\n\nReasons:" +
                                       err, "Clear Log Finished");
                    }
                }
            } else {
                Dialogs.ErrorOK(this, "Cannot clear the log files because they could not be located.",
                                "Clear Log Error");
            }
        }

        private void ChkLogAutoClear_CheckedChanged(object sender, EventArgs e)
        {
            this.pnlLogSizeThreshold.Enabled = this.chkLogAutoClear.Checked;
            this.txtAutoClearSize.Focus();
            this.txtAutoClearSize.SelectAll();
            this.ControlPanel.AppConfig.SetOption<bool>(AppConstants.LogAutoClear, this.chkLogAutoClear.Checked);
        }
        #endregion

        private bool Save_Validate()
        {
            bool result = true;
            if (this.txtAutoClearSize.Enabled) {
                result = this.txtAutoClearSize.ValidateInput(true);
            }
            return result;
        }

        private void Save_Start()
        {
            var cfg = this.ControlPanel.AppConfig;
            cfg.SetOption<int>(AppConstants.LogAutoClearThreshold, this.txtAutoClearSize.Value);
            cfg.Save(false);
        }
    }
}
