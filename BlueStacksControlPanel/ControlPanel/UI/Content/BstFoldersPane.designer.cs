﻿namespace BlueStacks.ControlPanel.UI.Content
{
    partial class BstFoldersPane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAdd = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.btnRemove = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.btnSave = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.treeFolders = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lnkUndo = new System.Windows.Forms.LinkLabel();
            this.txtFolderPath = new System.Windows.Forms.TextBox();
            this.txtFolderName = new System.Windows.Forms.TextBox();
            this.chkWritable = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.btnRemove);
            this.panel2.Location = new System.Drawing.Point(8, 218);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(163, 58);
            this.panel2.TabIndex = 11;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(12, 15);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(68, 32);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Tag = "Add a new shared folder. This folder won\'t be immediately written to the registry" +
                ".";
            this.btnAdd.Text = "Add New";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnRemove.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.btnRemove.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRemove.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(86, 15);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(65, 32);
            this.btnRemove.TabIndex = 3;
            this.btnRemove.Tag = "Remove the selected shared folder. This folder won\'t be immediately removed from " +
                "the registry.";
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(326, 233);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 32);
            this.btnSave.TabIndex = 7;
            this.btnSave.Tag = "Performs all changes made here to the registry. Save only when you are done editi" +
                "ng all folders.";
            this.btnSave.Text = "Save All Changes";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // treeFolders
            // 
            this.treeFolders.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.treeFolders.Location = new System.Drawing.Point(8, 35);
            this.treeFolders.Name = "treeFolders";
            this.treeFolders.Size = new System.Drawing.Size(163, 182);
            this.treeFolders.TabIndex = 1;
            this.treeFolders.Tag = "All pre-installed and user-made shared folders are shown here.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add/Edit/Remove Shared Folders:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lnkUndo);
            this.panel1.Controls.Add(this.txtFolderPath);
            this.panel1.Controls.Add(this.txtFolderName);
            this.panel1.Controls.Add(this.chkWritable);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(168, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 182);
            this.panel1.TabIndex = 10;
            // 
            // lnkUndo
            // 
            this.lnkUndo.ActiveLinkColor = System.Drawing.Color.Silver;
            this.lnkUndo.AutoSize = true;
            this.lnkUndo.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lnkUndo.LinkColor = System.Drawing.Color.WhiteSmoke;
            this.lnkUndo.Location = new System.Drawing.Point(189, 152);
            this.lnkUndo.Name = "lnkUndo";
            this.lnkUndo.Size = new System.Drawing.Size(84, 13);
            this.lnkUndo.TabIndex = 14;
            this.lnkUndo.TabStop = true;
            this.lnkUndo.Tag = "Reverts all changes made to the shared folder.";
            this.lnkUndo.Text = "Undo Changes";
            this.lnkUndo.VisitedLinkColor = System.Drawing.Color.WhiteSmoke;
            // 
            // txtFolderPath
            // 
            this.txtFolderPath.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtFolderPath.Location = new System.Drawing.Point(17, 115);
            this.txtFolderPath.Name = "txtFolderPath";
            this.txtFolderPath.Size = new System.Drawing.Size(256, 22);
            this.txtFolderPath.TabIndex = 5;
            this.txtFolderPath.Tag = "Path to the folder in your filesystem.";
            // 
            // txtFolderName
            // 
            this.txtFolderName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.txtFolderName.Location = new System.Drawing.Point(17, 56);
            this.txtFolderName.Name = "txtFolderName";
            this.txtFolderName.Size = new System.Drawing.Size(148, 22);
            this.txtFolderName.TabIndex = 4;
            this.txtFolderName.Tag = "The name that will appear on the Android Device when mounted.";
            // 
            // chkWritable
            // 
            this.chkWritable.AutoSize = true;
            this.chkWritable.Location = new System.Drawing.Point(17, 151);
            this.chkWritable.Name = "chkWritable";
            this.chkWritable.Size = new System.Drawing.Size(70, 17);
            this.chkWritable.TabIndex = 6;
            this.chkWritable.Tag = "When checked, allows to create or delete content inside the shared folder from th" +
                "e Android Device.";
            this.chkWritable.Text = "Writable";
            this.chkWritable.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(9, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Folder path:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(3, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Edit Information:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(9, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Shared folder name:";
            // 
            // BstFoldersPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.treeFolders);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "BstFoldersPane";
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView treeFolders;
        private Controls.BstButton btnAdd;
        private Controls.BstButton btnSave;
        private Controls.BstButton btnRemove;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkWritable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtFolderPath;
        private System.Windows.Forms.TextBox txtFolderName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel lnkUndo;
    }
}
