using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using BlueStacks.Management.Config;
using BlueStacks.Management.Interop;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI.Content
{
    internal sealed partial class BstLauncherPane : BstContentPane
    {
        private static readonly ProcessStartInfo LauncherInfo;

        static BstLauncherPane()
        {
            LauncherInfo = new ProcessStartInfo("bstlauncher.exe", "/logpipe");
            LauncherInfo.RedirectStandardOutput = true;
            LauncherInfo.UseShellExecute = false;
        }

        private delegate void TestRunFinish();
        private delegate void TestRunDialog(string msg, string title, int type);
        private delegate void StatusTextChange(string text);

        private List<string> testRunDebug;

        private TestRunFinish testRunFinish;
        private TestRunDialog testRunDialog;
        private StatusTextChange statusTextChange;

        public BstLauncherPane() : base()
        {
            this.InitializeComponent();
            this.txtMaxAttachTime.MinValue = AppConstants.MinPollTime;
            this.txtMaxAttachTime.MaxValue = AppConstants.MaxPollTime;
            this.testRunFinish = new TestRunFinish(this.FinishTestRun);
            this.testRunDialog = new TestRunDialog(this.ShowTestRunDialog);
            this.statusTextChange = new StatusTextChange(this.SetTestStatusText);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.ControlPanel.RegisterStatusDescription(this.chkDisableService);
            this.ControlPanel.RegisterStatusDescription(this.chkExitWindow);
            this.ControlPanel.RegisterStatusDescription(this.chkMinimizeToTray);
            this.ControlPanel.RegisterStatusDescription(this.btnTest);
            this.ControlPanel.RegisterStatusDescription(this.btnSave);
            this.ControlPanel.RegisterStatusDescription(this.txtMaxAttachTime);
            this.ControlPanel.RegisterStatusDescription(this.lblMillisecondsTip);
            this.ControlPanel.RegisterSaveButton(this.btnSave, this, this.Save_Validate, this.Save_Start, null);
            var cfg = this.ControlPanel.AppConfig;
            this.chkDisableService.Checked = cfg.GetOption<bool>(AppConstants.DisableServicesOnExit, true);
            this.chkExitWindow.Checked = cfg.GetOption<bool>(AppConstants.ExitBlueStacksOnWindowClose);
            this.txtMaxAttachTime.Value = MathEx.Clamp(cfg.GetOption<int>(AppConstants.MaxProcessAttachTime, AppConstants.DefPollTime), AppConstants.MinPollTime, AppConstants.MaxPollTime);
            this.chkMinimizeToTray.Checked = cfg.GetOption<bool>(AppConstants.MinimizeToTray, true);
        }

        public override void OnApplicationClosing()
        {
            if (this.txtMaxAttachTime.ValidateInput(false)) {
                this.ControlPanel.AppConfig.SetOption<int>(AppConstants.MaxProcessAttachTime, this.txtMaxAttachTime.Value);
            }
        }

        private void CheckBoxSetting_CheckChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;
            string op;
            if (chk == this.chkDisableService) {
                op = AppConstants.DisableServicesOnExit;
            } else if (chk == this.chkExitWindow) {
                op = AppConstants.ExitBlueStacksOnWindowClose;
            } else {
                op = AppConstants.MinimizeToTray;
                this.ControlPanel.CplSource.EnableNotificationMode = chk.Checked;
            }
            this.ControlPanel.AppConfig.SetOption<bool>(op, chk.Checked);
        }

        #region Launcher Testing Handler
        private void btnTest_Click(object sender, EventArgs e)
        {
            if (this.btnTest.Enabled) {
                this.btnTest.Enabled = false;
                this.SetTestStatusText("Please wait...");
                this.ControlPanel.CplSource.AllowWindowClose = false;
                this.ControlPanel.CplSource.DenyWindowCloseReason = "Launcher Test Run in progress.\n\n" +
                                                                    "The Control Panel cannot close until this " +
                                                                    "is finished.";
                ThreadPool.QueueUserWorkItem(this.TestRun_Callback);
            }
        }

        private void TestRun_Callback(object state)
        {
            if (this.testRunDebug == null) {
                this.testRunDebug = new List<string>();
            } else {
                this.testRunDebug.Clear();
            }
            this.testRunDebug.Add("BlueStacks Control Panel: Launcher Test Run");
            this.testRunDebug.Add("Ran on: " + DateTime.Now);
            this.testRunDebug.Add("\r\nLog:");
            if (File.Exists(LauncherInfo.FileName)) {
                try {
                    using (var p = Process.Start(LauncherInfo)) {
                        string text = null;
                        while (!p.StandardOutput.EndOfStream) {
                            text = p.StandardOutput.ReadLine();
                            this.testRunDebug.Add(DateTime.Now + ": " + text);
                            this.BeginInvoke(this.statusTextChange, text);
                        }
                        string msg;
                        int type = 1;
                        switch (p.ExitCode) {
                            case BstLauncherCodes.ExitOK:
                                type = 0;
                                msg = "The launcher has finished executing.";
                                break;
                            case BstLauncherCodes.TaskKilled:
                                msg = "The launcher process was terminated by the user.";
                                this.BeginInvoke(this.statusTextChange, msg);
                                break;
                            case BstLauncherCodes.FatalError:
                                msg = "The launcher has crashed.\n\nMessage:\n" + text;
                                break;
                            case BstLauncherCodes.NoInstallDirectory:
                                msg = "The launcher could not locate BlueStacks' installation directory.";
                                break;
                            case BstLauncherCodes.LauncherAlreadyRunning:
                                msg = "The launcher could not execute because an instance was already running.";
                                break;
                            default:
                                msg = "The launcher closed due to an unknown reason.";
                                break;
                        }
                        this.testRunDebug.Add("\r\nLauncher Test Run finished: " + DateTime.Now);
                        this.testRunDebug.Add("Control Panel Message: " + msg);
                        msg += "\n\nYou can check the whole test run at the file: debug.log for more information.";
                        this.Invoke(this.testRunDialog, msg, "Test Run Finished" + ((type != 0) ? " with Errors" : ""), type);
                        p.Close();
                    }
                    File.WriteAllLines("debug.log", this.testRunDebug, System.Text.UTF8Encoding.UTF8);
                } catch (Exception e) {
                    this.Invoke(this.testRunDialog, "An unexpected error has occured while testing " +
                                "the launcher.\n\nError:\n" + e.Message, "Unexpected Error Occured", 2);
                    this.BeginInvoke(this.statusTextChange, "An unexpected error has occured.");
                }
            } else {
                this.Invoke(this.testRunDialog, "Could not find the launcher executable.", "File Not Found", 2);
                this.BeginInvoke(this.statusTextChange, "Launcher executable not found.");
            }
            this.BeginInvoke(this.testRunFinish);
        }

        private void SetTestStatusText(string text)
        {
            this.lblTestStatus.Text = text;
        }

        private void ShowTestRunDialog(string msg, string title, int type)
        {
            switch(type) {
                case 0:
                    Dialogs.InfoOK(this, msg, title);
                    break;
                case 1:
                    Dialogs.WarnOK(this, msg, title);
                    break;
                default:
                    Dialogs.ErrorOK(this, msg, title);
                    break;
            }
        }

        private void FinishTestRun()
        {
            this.btnTest.Enabled = true;
            this.ControlPanel.CplSource.AllowWindowClose = true;
            this.ControlPanel.CplSource.DenyWindowCloseReason = null;
        }
        #endregion

        private bool Save_Validate()
        {
            return this.txtMaxAttachTime.ValidateInput(true);
        }

        private void Save_Start()
        {
            var cfg = this.ControlPanel.AppConfig;
            cfg.SetOption<int>(AppConstants.MaxProcessAttachTime, this.txtMaxAttachTime.Value);
            cfg.Save(false);
        }
    }
}
