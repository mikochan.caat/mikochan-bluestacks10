﻿namespace BlueStacks.ControlPanel.UI.Content
{
    partial class BstLauncherPane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.chkDisableService = new System.Windows.Forms.CheckBox();
            this.chkExitWindow = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaxAttachTime = new BlueStacks.ControlPanel.UI.Controls.BstNumTextBox();
            this.btnSave = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.lblMillisecondsTip = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnTest = new BlueStacks.ControlPanel.UI.Controls.BstButton();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTestStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chkMinimizeToTray = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(290, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Change the launcher\'s behavior when BlueStacks exits:";
            // 
            // chkDisableService
            // 
            this.chkDisableService.AutoSize = true;
            this.chkDisableService.BackColor = System.Drawing.Color.Transparent;
            this.chkDisableService.Location = new System.Drawing.Point(11, 31);
            this.chkDisableService.Name = "chkDisableService";
            this.chkDisableService.Size = new System.Drawing.Size(238, 17);
            this.chkDisableService.TabIndex = 1;
            this.chkDisableService.Tag = "Disables services re-enabled by BlueStacks. Takes effect when BlueStacks fully cl" +
                "oses.";
            this.chkDisableService.Text = "Disable services re-enabled by BlueStacks";
            this.chkDisableService.UseVisualStyleBackColor = false;
            this.chkDisableService.CheckedChanged += new System.EventHandler(this.CheckBoxSetting_CheckChanged);
            // 
            // chkExitWindow
            // 
            this.chkExitWindow.AutoSize = true;
            this.chkExitWindow.BackColor = System.Drawing.Color.Transparent;
            this.chkExitWindow.Location = new System.Drawing.Point(11, 53);
            this.chkExitWindow.Name = "chkExitWindow";
            this.chkExitWindow.Size = new System.Drawing.Size(308, 17);
            this.chkExitWindow.TabIndex = 2;
            this.chkExitWindow.Tag = "Fully closes BlueStacks when its main window (HD-Frontend.exe) has been closed.";
            this.chkExitWindow.Text = "Exit BlueStacks when its main window has been closed";
            this.chkExitWindow.UseVisualStyleBackColor = false;
            this.chkExitWindow.CheckedChanged += new System.EventHandler(this.CheckBoxSetting_CheckChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(5, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Launcher process attacher behavior:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(17, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Max Attach Time:";
            // 
            // txtMaxAttachTime
            // 
            this.txtMaxAttachTime.AcceptButton = this.btnSave;
            this.txtMaxAttachTime.AcceptsReturn = true;
            this.txtMaxAttachTime.Location = new System.Drawing.Point(112, 103);
            this.txtMaxAttachTime.MaxValue = 120000;
            this.txtMaxAttachTime.MinValue = 5000;
            this.txtMaxAttachTime.Name = "txtMaxAttachTime";
            this.txtMaxAttachTime.PropertyName = "Max Attach Time";
            this.txtMaxAttachTime.Size = new System.Drawing.Size(51, 22);
            this.txtMaxAttachTime.TabIndex = 3;
            this.txtMaxAttachTime.Tag = "Sets the maximum time to attach the launcher to the BlueStacks process.";
            this.txtMaxAttachTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxAttachTime.Value = 60000;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(326, 233);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(117, 32);
            this.btnSave.TabIndex = 6;
            this.btnSave.Tag = "Saves your options. Takes effect the next time you run the launcher.";
            this.btnSave.Text = "Save Settings";
            this.btnSave.UseVisualStyleBackColor = false;
            // 
            // lblMillisecondsTip
            // 
            this.lblMillisecondsTip.AutoSize = true;
            this.lblMillisecondsTip.BackColor = System.Drawing.Color.Transparent;
            this.lblMillisecondsTip.Location = new System.Drawing.Point(166, 108);
            this.lblMillisecondsTip.Name = "lblMillisecondsTip";
            this.lblMillisecondsTip.Size = new System.Drawing.Size(93, 13);
            this.lblMillisecondsTip.TabIndex = 2;
            this.lblMillisecondsTip.Tag = "1000 milliseconds (ms) = 1 second";
            this.lblMillisecondsTip.Text = "milliseconds (ms)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(5, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Launcher testing:";
            // 
            // btnTest
            // 
            this.btnTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(72)))), ((int)(((byte)(72)))));
            this.btnTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTest.ForeColor = System.Drawing.Color.White;
            this.btnTest.Location = new System.Drawing.Point(20, 157);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(113, 23);
            this.btnTest.TabIndex = 4;
            this.btnTest.Tag = "Test and check the status of the launcher from the Control Panel.";
            this.btnTest.Text = "Test Now";
            this.btnTest.UseVisualStyleBackColor = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 7.75F);
            this.label6.Location = new System.Drawing.Point(30, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Status:";
            // 
            // lblTestStatus
            // 
            this.lblTestStatus.AutoEllipsis = true;
            this.lblTestStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblTestStatus.Font = new System.Drawing.Font("Segoe UI", 7.75F);
            this.lblTestStatus.Location = new System.Drawing.Point(70, 186);
            this.lblTestStatus.Name = "lblTestStatus";
            this.lblTestStatus.Size = new System.Drawing.Size(383, 13);
            this.lblTestStatus.TabIndex = 2;
            this.lblTestStatus.Text = "Not running.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(5, 213);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Control panel options:";
            // 
            // chkMinimizeToTray
            // 
            this.chkMinimizeToTray.AutoSize = true;
            this.chkMinimizeToTray.BackColor = System.Drawing.Color.Transparent;
            this.chkMinimizeToTray.Location = new System.Drawing.Point(11, 233);
            this.chkMinimizeToTray.Name = "chkMinimizeToTray";
            this.chkMinimizeToTray.Size = new System.Drawing.Size(130, 17);
            this.chkMinimizeToTray.TabIndex = 5;
            this.chkMinimizeToTray.Tag = "When checked, the Control Panel will go to your notification area when minimized." +
                "";
            this.chkMinimizeToTray.Text = "Minimize this to tray";
            this.chkMinimizeToTray.UseVisualStyleBackColor = false;
            this.chkMinimizeToTray.CheckedChanged += new System.EventHandler(this.CheckBoxSetting_CheckChanged);
            // 
            // BstLauncherPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.txtMaxAttachTime);
            this.Controls.Add(this.lblTestStatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblMillisecondsTip);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkMinimizeToTray);
            this.Controls.Add(this.chkExitWindow);
            this.Controls.Add(this.chkDisableService);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BstLauncherPane";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkDisableService;
        private System.Windows.Forms.CheckBox chkExitWindow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private BlueStacks.ControlPanel.UI.Controls.BstNumTextBox txtMaxAttachTime;
        private System.Windows.Forms.Label lblMillisecondsTip;
        private System.Windows.Forms.Label label5;
        private BlueStacks.ControlPanel.UI.Controls.BstButton btnTest;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTestStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkMinimizeToTray;
        private BlueStacks.ControlPanel.UI.Controls.BstButton btnSave;
    }
}
