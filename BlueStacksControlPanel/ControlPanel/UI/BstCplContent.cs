using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BlueStacks.ControlPanel.UI.Content;
using BlueStacks.ControlPanel.UI.Controls;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI
{
    internal sealed partial class BstCplContent : UserControl
    {
        public delegate bool ValidateSave();
        public delegate void SaveCallback();

        public ICplSource CplSource
        {
            get;
            private set;
        }

        private BstContentPane[] contentPanes;
        private BstCplButton currentCplButton;

        private Dictionary<Button, Tuple<BstContentPane, ValidateSave, SaveCallback, SaveCallback, string>> saveTable;
        private BstContentPane savingPane;
        private Timer saveCooldownTimer;
        private object[] saveButtonData;

        public BstCplContent(ICplSource cplSrc) : base()
        {
            this.InitializeComponent();
            this.CplSource = cplSrc;
            this.InitAnchors();
            this.contentPanes = new BstContentPane[] {
                new BstLauncherPane(), new BstDevicePane(), new BstFoldersPane(),
                new BstMiscPane(), new BstAboutPane()
            };
            this.pnlContentHolder.Controls.AddRange(this.contentPanes);
            this.RegisterStatusDescription(this.btnCplLauncher);
            this.RegisterStatusDescription(this.btnCplDevice);
            this.RegisterStatusDescription(this.btnCplFolders);
            this.RegisterStatusDescription(this.btnCplMisc);
            this.RegisterStatusDescription(this.btnCplAbout);
            this.saveTable = new Dictionary<Button, Tuple<BstContentPane, ValidateSave, SaveCallback, SaveCallback, string>>();
            this.saveCooldownTimer = new Timer();
            this.saveCooldownTimer.Interval = 650;
            this.saveCooldownTimer.Tick += this.ContentPaneSaveCooldown_Tick;
            this.saveButtonData = new object[2];
        }

        private BstCplContent() : base()
        {
            this.InitializeComponent();
            this.InitAnchors();
        }

        public void RegisterStatusDescription(Control ctrl)
        {
            ctrl.MouseEnter += this.Control_StatusDescriptionChangeEnter;
            ctrl.MouseLeave += this.Control_StatusDescriptionChangeLeave;
            if (ctrl is TextBox) {
                var tb = ctrl as TextBox;
                tb.Enter += this.TextBox_StatusDescriptionChangeEnter;
                tb.Leave += this.TextBox_StatusDescriptionChangeLeave;
            }
        }

        public void RegisterSaveButton(Button btn, BstContentPane pane, ValidateSave validator,
                                       SaveCallback onSave, SaveCallback onSaveEnd)
        {
            var data = Tuple.Create<BstContentPane, ValidateSave, SaveCallback, SaveCallback, string>(pane, validator, onSave, onSaveEnd, btn.Text);
            this.saveTable.Add(btn, data);
            btn.Click += this.ContentPaneSaveButton_Click;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.ParentForm.FormClosing += new FormClosingEventHandler(this.ParentForm_FormClosing);
            this.PerformLoad();
            this.btnCplLauncher.PerformClick();
        }

        #region Disposer
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (this.components != null) {
                    this.components.Dispose();
                }
                this.saveCooldownTimer.Dispose();
                this.saveTable.Clear();
                this.contentPanes = null;
                this.currentCplButton = null;
                this.saveTable = null;
                this.savingPane = null;
                this.saveButtonData = null;
                this.PerformCleanup();
            }
            base.Dispose(disposing);
        }
        #endregion

        private void InitAnchors()
        {
            this.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
        }

        #region Status Bar Support
        private void ChangeStatusDescription(object sender, bool setTag)
        {
            object tag = (sender as Control).Tag;
            this.statusLabel.Text = (tag ?? "Ready.").ToString();
            if (setTag) {
                this.statusLabel.Tag = tag;
            }
        }

        private void ResetStatusDescription()
        {
            this.statusLabel.Text = (this.statusLabel.Tag ?? "Ready.").ToString();
        }

        private void Control_StatusDescriptionChangeEnter(object sender, EventArgs e)
        {
            this.ChangeStatusDescription(sender, false);
        }

        private void Control_StatusDescriptionChangeLeave(object sender, EventArgs e)
        {
            this.ResetStatusDescription();
        }

        private void TextBox_StatusDescriptionChangeEnter(object sender, EventArgs e)
        {
            this.ChangeStatusDescription(sender, true);
        }

        private void TextBox_StatusDescriptionChangeLeave(object sender, EventArgs e)
        {
            this.statusLabel.Tag = null;
        }
        #endregion

        #region Configuration Save Cooldown Support
        private void ContentPaneSaveButton_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            var data = this.saveTable[btn];
            if (data.Item2 != null) {
                if (!data.Item2.Invoke()) {
                    return;
                }
            }
            this.savingPane = data.Item1;
            this.pnlCplButtonHolder.Enabled = false;
            if (this.savingPane != null) {
                this.savingPane.Enabled = false;
            }
            btn.Text = "Saving...";
            if (data.Item3 != null) {
                data.Item3.Invoke();
            }
            this.saveButtonData[0] = btn;
            this.saveButtonData[1] = data.Item5;
            this.saveCooldownTimer.Tag = data.Item4;
            this.saveCooldownTimer.Start();
        }

        private void ContentPaneSaveCooldown_Tick(object sender, EventArgs e)
        {
            this.saveCooldownTimer.Stop();
            var callback = this.saveCooldownTimer.Tag as SaveCallback;
            if (callback != null) {
                callback.Invoke();
            }
            (this.saveButtonData[0] as Button).Text = this.saveButtonData[1] as string;
            if (this.savingPane != null) {
                this.savingPane.Enabled = true;
            }
            this.pnlCplButtonHolder.Enabled = true;
            this.saveCooldownTimer.Tag = null;
        }
        #endregion

        private void NavCplButton_Clicked(object sender, EventArgs e)
        {
            var btn = sender as BstCplButton;
            if (btn != this.currentCplButton) {
                if (this.contentPanes != null) {
                    this.contentPanes[btn.TagId].Visible = true;
                }
                if (this.currentCplButton != null) {
                    this.currentCplButton.Selected = false;
                    this.contentPanes[this.currentCplButton.TagId].Visible = false;
                }
                this.currentCplButton = btn;
                btn.Selected = true;
                this.statusLabel.Tag = null;
                this.ResetStatusDescription();
            }
        }

        private void ParentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.CplSource.AllowWindowClose) {
                foreach (var pane in this.contentPanes) {
                    if (pane.HasControlPanel) {
                        pane.OnApplicationClosing();
                    }
                }
                this.PerformOnClosing();
            } else {
                string msg;
                if (!String.IsNullOrWhiteSpace(this.CplSource.DenyWindowCloseReason)) {
                    msg = this.CplSource.DenyWindowCloseReason;
                } else {
                    msg = "Cannot close the Control Panel until all operations are finished.";
                }
                Dialogs.WarnOK(this, msg, "Control Panel is Busy");
                e.Cancel = true;
            }
        }
    }
}
