using System;
using System.ComponentModel;
using System.Windows.Forms;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI.Controls
{
    internal sealed class BstNumTextBox : TextBox
    {
        private static readonly ContextMenuStrip TextboxContextMenuStrip = new ContextMenuStrip();

        private const int Num0 = (int)Keys.NumPad0;
        private const int Dig0 = (int)Keys.D0;

        private Button acceptButton;

        private int defVal;
        private int maxVal;
        private int minVal;

        [Browsable(false)]
        public override ContextMenuStrip ContextMenuStrip
        {
            get
            {
                return null;
            }
            set
            {
                throw new InvalidOperationException("Cannot change context menu.");
            }
        }

        [Browsable(false)]
        public override ContextMenu ContextMenu
        {
            get
            {
                return null;
            }
            set
            {
                throw new InvalidOperationException("Cannot change context menu.");
            }
        }

        /// <summary>
        /// Use the Value property of this control instead and invoke ToString().
        /// </summary>
        [Browsable(false)]
        [Obsolete("Use the Value property of this control instead and invoke ToString().")]
#pragma warning disable 0809
        public override string Text
#pragma warning restore 0809
        {
            get
            {
                return "";
            }

            set
            {
                // Unsupported but cannot throw exception
                // due to Forms designer quirks.
            }
        }

        [Description("Name of the property that this control is meant to hold.")]
        public string PropertyName
        {
            get;
            set;
        }

        [Description("Maximum integer value for this control.")]
        public int MaxValue
        {
            get
            {
                return this.maxVal;
            }

            set
            {
                if (value < this.minVal) {
                    this.minVal = value;
                }
                if (this.defVal > value) {
                    this.defVal = value;
                }
                this.maxVal = value;
            }
        }

        [Description("Minimum integer value for this control.")]
        public int MinValue
        {
            get
            {
                return this.minVal;
            }

            set
            {
                if (value < 0) {
                    throw new ArgumentException("Only positive values are allowed.");
                }
                if (value > this.maxVal) {
                    this.maxVal = value;
                }
                if (this.defVal < value) {
                    this.defVal = value;
                }
                this.minVal = value;
            }
        }

        [Description("Default value for this control.")]
        public int Value
        {
            get
            {
                return this.defVal;
            }

            set
            {
                if (value < this.minVal || value > this.maxVal) {
                    throw new ArgumentException("Value out of range. Minimum: " + this.minVal + " " +
                                                "Maximum: " + this.maxVal);
                }
                this.defVal = value;
                this.SetText(value.ToString());
            }
        }

        [Description("The button that will be clicked when the user types a return character.")]
        public Button AcceptButton
        {
            get
            {
                return this.acceptButton;
            }

            set
            {
                this.acceptButton = value;
                this.AcceptsReturn = true;
            }
        }

        public BstNumTextBox() : base()
        {
            this.TextAlign = HorizontalAlignment.Right;
            base.ContextMenuStrip = TextboxContextMenuStrip;
            base.Text = "0";
        }

        public bool ValidateInput(bool showDialog)
        {
            int val;
            bool result = ValidateInput(this, showDialog, out val);
            if (result) {
                this.defVal = val;
            }
            return result;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            var key = e.KeyCode;
            int keyVal = e.KeyValue;
            e.SuppressKeyPress = !(Char.IsNumber((char)((keyVal < Num0) ? keyVal : keyVal - Dig0)) ||
                                 key == Keys.Delete || key == Keys.Back || key == Keys.Up ||
                                 key == Keys.Down || key == Keys.Left || key == Keys.Right ||
                                 (e.Modifiers.HasFlag(Keys.Control) && key == Keys.A));
            if ((key == Keys.Enter || key == Keys.Return) && this.acceptButton != null) {
                this.acceptButton.PerformClick();
            }
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            this.SelectionStart = this.GetText().Length;
        }

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            if (this.GetText().Length == 0) {
                this.SetText(this.Value.ToString());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                this.acceptButton = null;
            }
            base.Dispose(disposing);
        }

        private void SetText(string txt)
        {
            base.Text = txt;
        }

        private string GetText()
        {
            return base.Text;
        }

        private static bool ValidateInput(BstNumTextBox txtBox, bool showDialog, out int val)
        {
            bool result = false;
            string txt = txtBox.GetText();
            val = 0;
            if (txt.Length > 0) {
                try {
                    val = Convert.ToInt32(txt);
                    if (val >= txtBox.MinValue && val <= txtBox.MaxValue) {
                        result = true;
                    } else {
                        DisplayOutOfRangeError(txtBox, showDialog);
                    }
                } catch (FormatException) {
                    DisplayValidateError(txtBox, txtBox.PropertyName + " only accepts numerical values.",
                                         "Invalid Input", showDialog);
                } catch (OverflowException) {
                    DisplayOutOfRangeError(txtBox, showDialog);
                }
            } else {
                DisplayValidateError(txtBox, txtBox.PropertyName + " cannot be empty.",
                                     "Invalid Input", showDialog);
            }
            return result;
        }

        private static void DisplayOutOfRangeError(BstNumTextBox txtBox, bool show)
        {
            if (!show) {
                return;
            }
            DisplayValidateError(txtBox, "Value of " + txtBox.PropertyName + " is out of range.\n" +
                                 "Minimum: " + txtBox.MinValue + " Maximum: " + txtBox.MaxValue,
                                 "Invalid Value", true);
        }

        private static void DisplayValidateError(BstNumTextBox txtBox, string msg, string title, bool show)
        {
            if (show) {
                Dialogs.ErrorOK(msg, title);
                txtBox.Focus();
            }
        }
    }
}
