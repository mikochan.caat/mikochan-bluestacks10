using System.Drawing;
using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI.Controls
{
    internal sealed class BstButton : Button
    {
        private static readonly Color DefBackColor = Color.FromArgb(72, 72, 72);
        private static readonly Color DefMouseDownColor = Color.FromArgb(64, 64, 64);

        public BstButton() : base()
        {
            this.BackColor = DefBackColor;
            this.FlatAppearance.BorderColor = Color.WhiteSmoke;
            this.FlatAppearance.MouseDownBackColor = DefMouseDownColor;
            this.FlatAppearance.MouseOverBackColor = Color.DimGray;
            this.FlatStyle = FlatStyle.Flat;
            this.ForeColor = Color.White;
            this.UseVisualStyleBackColor = false;
        }
    }
}
