using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI.Controls
{
    internal sealed class BstCplButton : Button
    {
        private static readonly Color BGColorNormal = Color.FromArgb(72, 72, 72);

        private Color backColorSelected;
        private Color borderColor;
        private bool selected;

        [Browsable(false)]
        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
            }
        }

        [Browsable(false)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        [Description("Text color when not selected.")]
        public Color ForeColorNormal
        {
            get;
            set;
        }

        [Description("Text color when selected.")]
        public Color ForeColorSelected
        {
            get;
            set;
        }

        [Description("Background color when not selected.")]
        public Color BackColorNormal
        {
            get;
            set;
        }

        [Description("Background color when selected or held down.")]
        public Color BackColorSelected
        {
            get
            {
                return this.backColorSelected;
            }

            set
            {
                this.backColorSelected = value;
                this.FlatAppearance.MouseDownBackColor = value;
            }
        }

        [Description("Border color when not selected.")]
        public Color BorderColor
        {
            get
            {
                return this.borderColor;
            }

            set
            {
                this.borderColor = value;
                this.FlatAppearance.BorderColor = value;
                this.FlatAppearance.MouseOverBackColor = value;
            }
        }

        [Description("Changes button state and appearance.")]
        public bool Selected
        {
            get
            {
                return this.selected;
            }

            set
            {
                this.selected = value;
                this.SetButtonAppearance();
            }
        }

        [Description("General purpose integer tag.")]
        public int TagId
        {
            get;
            set;
        }

        public BstCplButton() : base()
        {
            this.ForeColorNormal = Color.WhiteSmoke;
            this.ForeColorSelected = Color.White;
            this.BackColorNormal = BGColorNormal;
            this.BackColorSelected = Color.DimGray;
            this.ForeColor = this.ForeColorNormal;
            this.BackColor = this.BackColorNormal;
            this.BorderColor = Color.Gray;
            this.FlatStyle = FlatStyle.Flat;
            this.FlatAppearance.BorderSize = 1;
            this.UseVisualStyleBackColor = false;
        }

        private void SetButtonAppearance()
        {
            Color backColor;
            Color foreColor;
            Color borderColor;
            int borderSize;
            if (this.selected) {
                borderColor = backColor = this.BackColorSelected;
                foreColor = this.ForeColorSelected;
                borderSize = 0;
            } else {
                backColor = this.BackColorNormal;
                foreColor = this.ForeColorNormal;
                borderColor = this.BorderColor;
                borderSize = 1;
            }
            this.BackColor = backColor;
            this.ForeColor = foreColor;
            var flat = this.FlatAppearance;
            flat.BorderColor = borderColor;
            flat.MouseOverBackColor = borderColor;
            flat.BorderSize = borderSize;
        }
    }
}
