﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI.Controls
{
    internal sealed class BstBackgroundPanel : Panel
    {
        [Browsable(true)]
        [Description("The background image to be painted.")]
        public Image PaintedImage
        {
            get;
            set;
        }

        public BstBackgroundPanel() : base()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);
            this.BackColor = Color.Transparent;
        }

        public void DisposeImage()
        {
            if (this.PaintedImage != null) {
                this.PaintedImage.Dispose();
                this.PaintedImage = null;
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
            if (this.PaintedImage != null) {
                e.Graphics.DrawImageUnscaled(this.PaintedImage, 0, 0);
            }
        }
    }
}
