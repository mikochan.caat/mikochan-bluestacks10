﻿namespace BlueStacks.ControlPanel.UI
{
    partial class BstCplForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.appSkin = new BlueStacks.ControlPanel.UI.AppFormSkin();
            this.trayMenu = new BlueStacks.ControlPanel.UI.PopupMenu();
            this.trayMenuItemShow = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.trayMenuItemExit = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // trayIcon
            // 
            this.trayIcon.Text = "notifyIcon1";
            this.trayIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseClick);
            // 
            // appSkin
            // 
            this.appSkin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.appSkin.FixedSingle = false;
            this.appSkin.Location = new System.Drawing.Point(0, 0);
            this.appSkin.Name = "appSkin";
            this.appSkin.Size = new System.Drawing.Size(284, 261);
            this.appSkin.Stretch = false;
            this.appSkin.TabIndex = 0;
            // 
            // trayMenu
            // 
            this.trayMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.trayMenuItemShow,
            this.menuItem1,
            this.trayMenuItemExit});
            // 
            // trayMenuItemShow
            // 
            this.trayMenuItemShow.Index = 0;
            this.trayMenuItemShow.Text = "Show";
            this.trayMenuItemShow.Click += new System.EventHandler(this.TrayMenuItemShow_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 1;
            this.menuItem1.Text = "-";
            // 
            // trayMenuItemExit
            // 
            this.trayMenuItemExit.Index = 2;
            this.trayMenuItemExit.Text = "Exit";
            this.trayMenuItemExit.Click += new System.EventHandler(this.TrayMenuItemExit_Click);
            // 
            // BstCplForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.appSkin);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(163, 38);
            this.Name = "BstCplForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);

        }

        #endregion

        private AppFormSkin appSkin;
        private System.Windows.Forms.NotifyIcon trayIcon;
        private PopupMenu trayMenu;
        private System.Windows.Forms.MenuItem trayMenuItemShow;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem trayMenuItemExit;
    }
}