using System;
using System.Reflection;
using System.Windows.Forms;
using BlueStacks.Management.Interop;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI
{
    internal partial class BstCplForm : Form, SingleInstanceApp.IForm, ICplSource
    {
        public string SourceName
        {
            get
            {
                return "BlueStacks Control Panel";
            }
        }

        public string SourceVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public bool EnableNotificationMode
        {
            get;
            set;
        }

        public bool AllowWindowClose
        {
            get
            {
                return this.allowWindowClose;
            }

            set
            {
                this.allowWindowClose = value;
            }
        }

        public string DenyWindowCloseReason
        {
            get;
            set;
        }

        private bool allowWindowClose = true;

        public BstCplForm()
        {
            this.InitializeComponent();
            this.Icon = this.trayIcon.Icon = ExeIcons.ControlPanelIcon;
            this.Text = this.trayIcon.Text = this.SourceName + " v" + this.SourceVersion;
            this.trayIcon.ContextMenu = this.trayMenu;
            BstCplContent cpl = new BstCplContent(this);
            var cplLoc = cpl.Location;
            cplLoc.X = 8;
            cplLoc.Y = 30;
            cpl.Location = cplLoc;
            this.Width = cpl.Width + 16;
            this.Height = cpl.Height + 38;
            this.components.Add(cpl);
            this.Controls.Add(cpl);
        }

        public void HandleNextInstance()
        {
            this.ReactivateWindow();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style |= Win32Methods.WS_MINIMIZEBOX;
                cp.ClassStyle |= Win32Methods.CS_DBLCLKS;
                return cp;
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Win32Methods.WM_SYSCOMMAND) {
                if ((m.WParam.ToInt32() & 0xFFF0) == Win32Methods.SC_MINIMIZE && this.EnableNotificationMode) {
                    this.Visible = false;
                    this.trayIcon.Visible = true;
                    return;
                }
            }
            base.WndProc(ref m);
        }

        private void ReactivateWindow()
        {
            if (!this.Visible) {
                this.Visible = true;
            }
            this.trayIcon.Visible = false;
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }

        private void TrayIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Middle) {
                this.ReactivateWindow();
            }
        }

        private void TrayMenuItemShow_Click(object sender, EventArgs e)
        {
            this.ReactivateWindow();
        }

        private void TrayMenuItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
