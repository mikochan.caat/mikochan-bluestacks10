using System;
using BlueStacks.Management;
using BlueStacks.Management.Config;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI
{
    internal sealed partial class BstCplContent
    {
        public AppConfig AppConfig
        {
            get
            {
                return this.appConfig;
            }
        }

        public BstProperties BstProperties
        {
            get
            {
                return this.bstProps;
            }
        }

        private AppConfig appConfig;
        private BstProperties bstProps;

        private void PerformLoad()
        {
            this.appConfig = new AppConfig(AppConstants.AppConfigPath);
            this.bstProps = new BstProperties();
            this.lblVersion.Text = " version " + this.CplSource.SourceVersion;
            this.lblOS.Text = Computer.OSName;
            this.lblRAM.Text = Computer.TotalRAM + " MB";
            if (this.bstProps.HasInstallDetails) {
                this.lblInstallDir.Text = this.bstProps.InstallPath;
                this.lblDataDir.Text = this.bstProps.DataPath;
                if (!this.bstProps.HasSystemSettings) {
                    this.btnCplDevice.Enabled = false;
                    Dialogs.WarnOK(this, "The application could not load any of BlueStacks' " +
                                   "settings.\n\nThe '" + this.btnCplDevice.Text + "' tab will " +
                                   "be inaccessible.", "Initialization Error");
                } else if (!this.bstProps.HasVideoSettings) {
                    Dialogs.WarnOK(this, "The application could not load BlueStacks' " +
                                   "video settings.\n\nThe '" + this.btnCplDevice.Text + "' tab will " +
                                   "have limited functionality.", "Initialization Error");
                }
            } else {
                this.lblInstallDir.Text = this.lblDataDir.Text = AppConstants.UnknownPath;
                Dialogs.ErrorOK(this, "The application could not load because the BlueStacks " +
                                "installation directory was not found.\n\nPress OK " +
                                "to exit the application.", "Initialization Error");
                this.ParentForm.Close();
            }
        }

        private void PerformOnClosing()
        {
            if (this.appConfig.HasPendingChanges) {
                if (Dialogs.WarnYesNo(this, "You have modified some settings in the " +
                                      "Control Panel but have not saved yet.\n\n" +
                                      "Do you want to save these changes?",
                                      "Settings Pending Changes")) {
                    this.appConfig.Save(true);
                }
            }
        }

        private void PerformCleanup()
        {
            this.bstProps.Dispose();
        }
    }
}
