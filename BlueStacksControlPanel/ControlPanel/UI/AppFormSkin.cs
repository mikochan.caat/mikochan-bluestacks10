﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

#region Copyright Notice
/*
    Copyright (c) 2013 jn4kim

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    http://jskin.codeplex.com/
    http://developert.com/
 */
#endregion
#region Edit Notice
/*
    Enhancement by Miko-chan (久利寿智安・ティグラオ) (mikochan.caat@gmail.com)
    Copyright (c) 2014-2015
 */
#endregion
namespace BlueStacks.ControlPanel.UI
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    internal partial class AppFormSkin : UserControl
    {

        #region - Property -
        private bool _Stretch = false;
        [Category("Stretch"), Description("If set to true, allows form resizing at runtime.")]
        public bool Stretch
        {
            get
            {
                return this._Stretch;
            }
            set
            {
                this._Stretch = value;
                if (value)
                    this.FixedSingle = false;
                this.forcePaint = true;
                this.Invalidate(false);
            }
        }

        private bool _FixedSingle = false;
        [Category("FixedSingle"), Description("If set to true, only the Close button appears on the form.")]
        public bool FixedSingle
        {
            get
            {
                return this._FixedSingle;
            }
            set
            {
                this._FixedSingle = value;
                if (value)
                    this.Stretch = false;
                this.forcePaint = true;
                this.Invalidate(false);
            }
        }
        #endregion

        #region - Resizing API & Const -
        [DllImport("User32")]
        private static extern int SendMessage(int hWnd, int hMsg, int wParam, int lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr DefWindowProc(IntPtr hWnd, uint uMsg, UIntPtr wParam, IntPtr lParam);

        [DllImport("User32")]
        private static extern int ReleaseCapture();

        private const uint WM_SYSCOMMAND = 0x112;
        private const uint SC_DRAG_RESIZEL = 0xF001;    // left resize
        private const uint SC_DRAG_RESIZER = 0xF002;    // right resize
        private const uint SC_DRAG_RESIZEU = 0xF003;    // upper resize
        private const uint SC_DRAG_RESIZEUL = 0xF004;   // upper-left resize
        private const uint SC_DRAG_RESIZEUR = 0xF005;   // upper-right resize
        private const uint SC_DRAG_RESIZED = 0xF006;    // down resize
        private const uint SC_DRAG_RESIZEDL = 0xF007;   // down-left resize
        private const uint SC_DRAG_RESIZEDR = 0xF008;   // down-right resize
        private const uint SC_DRAG_MOVE = 0xF012;       // move
        #endregion

        #region - FormSkin Setting -
        private const int _layoutW = 64;

        private const int _upCornerW = 10;  // ↖
        private const int _upH = 32;        // ↑
        private const int _downH = 10;      // ↓
        private const int _barH = 22;       // ↔

        private const int _btH = 21;
        private const int _btL = 113;
        private const int _btMinW = 29;
        private const int _btMaxW = 27;
        private const int _btExitW = 49;

        private const int _btRight = _btL - _btMinW - _btMaxW - _btExitW;

        private const int _TitleBarT = 30;
        #endregion

        #region - Button Variables -
        private bool _isMin = false, _isMax = false, _isExit = false;
        private bool _isMaxed = false;
        private bool _isReset = false;
        #endregion

        #region - Mouse API & Variables -
        private bool _isMouseIn = false;
        private bool _isClicked = false;
        private int _RightX = 0;
        private int _DownY = 0;
        #endregion

        #region - Private Methods -
        private const int _windowButtonRelX = _btExitW + _btMaxW + _btMinW;

        private Rectangle rect = new Rectangle();
        private Rectangle rectMin = new Rectangle(0, 0, _btMinW, _btH);
        private Rectangle rectMax = new Rectangle(0, 0, _btMaxW, _btH);
        private Rectangle rectClose = new Rectangle(0, 0, _btExitW, _btH);
        private Rectangle currRect;
        
        private Font titleFont = new Font("Segoe UI Semibold", 11f);
        private Icon icon;

        private int windowButtonX;
        private int titleTextY;

        private string cachedTitle;
        private float cachedTitleLength;

        private bool iconClicked;

        private bool forcePaint = true;
        private bool repaintButtonOnly = false;

        private void SetPaintRectangle(int x, int y, int w, int h)
        {
            this.rect.X = x;
            this.rect.Y = y;
            this.rect.Width = w;
            this.rect.Height = h;
        }

        private void ctlSkin_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.CompositingQuality = CompositingQuality.HighSpeed;
            e.Graphics.InterpolationMode = InterpolationMode.Bilinear;
            e.Graphics.SmoothingMode = SmoothingMode.None;
            if (!this.repaintButtonOnly || this.forcePaint) {
                //위
                this.SetPaintRectangle(0, 0, _upCornerW, _upH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, 0, 0, _upCornerW, _upH, GraphicsUnit.Pixel);
                this.SetPaintRectangle(_upCornerW, 0, this.Width - 2 * _upCornerW, _upH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, _upCornerW, 0, _layoutW - 2 * _upCornerW, _upH, GraphicsUnit.Pixel);
                this.SetPaintRectangle(this.Width - _upCornerW, 0, _upCornerW, _upH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, _layoutW - _upCornerW, 0, _upCornerW, _upH, GraphicsUnit.Pixel);

                //중간
                this.SetPaintRectangle(0, _upH, _upCornerW, this.Height - _upH - _downH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, 0, _upH, _upCornerW, _barH, GraphicsUnit.Pixel);
                this.SetPaintRectangle(_upCornerW, _upH, this.Width - 2 * _upCornerW, this.Height - _upH - _downH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, _upCornerW, _upH, _layoutW - 2 * _upCornerW, _barH, GraphicsUnit.Pixel);
                this.SetPaintRectangle(this.Width - _upCornerW, _upH, _upCornerW, this.Height - _upH - _downH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, _layoutW - _upCornerW, _upH, _upCornerW, _barH, GraphicsUnit.Pixel);

                //아래
                this.SetPaintRectangle(0, this.Height - _downH, _upCornerW, _downH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, 0, _layoutW - _downH, _upCornerW, _downH, GraphicsUnit.Pixel);
                this.SetPaintRectangle(_upCornerW, this.Height - _downH, this.Width - 2 * _upCornerW, _downH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, _upCornerW, _layoutW - _downH, _layoutW - 2 * _upCornerW, _downH, GraphicsUnit.Pixel);
                this.SetPaintRectangle(this.Width - _upCornerW, this.Height - _downH, _upCornerW, _downH);
                e.Graphics.DrawImage(pLayout.Image, this.rect, _layoutW - _upCornerW, _layoutW - _downH, _upCornerW, _downH, GraphicsUnit.Pixel);

                Form frm = (this.Parent as Form);

                //아이콘 그리기
                if (this.icon != frm.Icon) {
                    if (this.icon != null) {
                        this.icon.Dispose();
                    }
                    this.icon = new Icon(frm.Icon, 16, 16);
                }
                this.SetPaintRectangle(7, 7, 16, 16);
                e.Graphics.DrawIcon(this.icon, this.rect);

                if (this.cachedTitle != frm.Text) {
                    this.cachedTitle = frm.Text;
                    this.cachedTitleLength = TextRenderer.MeasureText(frm.Text, this.titleFont).Width;
                }
                e.Graphics.TextRenderingHint = TextRenderingHint.SystemDefault;
                int mid = (int)((this.Width / 2) - (this.cachedTitleLength / 2));
                TextFormatFlags textFlags = TextFormatFlags.EndEllipsis;
                if (mid + this.cachedTitleLength + 6 < this.windowButtonX) {
                    this.SetPaintRectangle(mid, this.titleTextY, this.windowButtonX - mid, this.titleFont.Height);
                } else {
                    this.SetPaintRectangle(25, this.titleTextY, this.windowButtonX - 25, this.titleFont.Height);
                    textFlags |= TextFormatFlags.HorizontalCenter;
                }
                TextRenderer.DrawText(e.Graphics, frm.Text, this.titleFont, this.rect, Color.WhiteSmoke, textFlags);
            }
            if (_isMouseIn)
            {   // 마우스 커서가 버튼 구간에 있음
                if (_RightX - _btRight <= _btExitW)
                {//종료 버튼 구간에 있음
                    _isExit = true;
                    _isMin = false;
                    _isMax = false;
                    if (_isClicked) {
                        this.SetPaintRectangle(Width - _btL + _btMinW + _btMaxW, 0, _btExitW, _btH);
                        e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW + _btMaxW, 2 * _btH, _btExitW, _btH, GraphicsUnit.Pixel);
                    } else {
                        this.SetPaintRectangle(Width - _btL + _btMinW + _btMaxW, 0, _btExitW, _btH);
                        e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW + _btMaxW, 1 * _btH, _btExitW, _btH, GraphicsUnit.Pixel);
                    }
                    //다른버튼들은 초기화
                    if (!FixedSingle)
                    {
                        if (Stretch) {
                            if (_isMaxed) {
                                this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 4 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                            } else {
                                this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 0, _btMaxW, _btH, GraphicsUnit.Pixel);
                            }
                        } else {
                            this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                            e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 3 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                        }
                        this.SetPaintRectangle(Width - _btL, 0, _btMinW, _btH);
                        e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 0, _btMinW, _btH, GraphicsUnit.Pixel);
                    }
                }
                else if (_RightX - _btRight <= _btExitW + _btMaxW)
                { //최대화 버튼 구간에 있음
                    _isMin = false;
                    _isExit = false;
                    if (!FixedSingle)
                    {
                        _isMax = true;
                        if (Stretch) {
                            if (_isClicked) {
                                if (_isMaxed) {
                                    this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                    e.Graphics.DrawImage(pButtons.Image, this.rect, _btMaxW * 2, 4 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                                } else {
                                    this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                    e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 2 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                                }
                            } else {
                                if (_isMaxed) {
                                    this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                    e.Graphics.DrawImage(pButtons.Image, this.rect, _btMaxW, 4 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                                } else {
                                    this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                    e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 1 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                                }
                            }
                        } else {
                            this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                            e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 3 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                        }
                        //다른버튼 초기화
                        this.SetPaintRectangle(Width - _btL, 0, _btMinW, _btH);
                        e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 0, _btMinW, _btH, GraphicsUnit.Pixel);
                    }
                    this.SetPaintRectangle(Width - _btL + _btMinW + _btMaxW, 0, _btExitW, _btH);
                    e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW + _btMaxW, 0, _btExitW, _btH, GraphicsUnit.Pixel);
                }
                else if (_RightX - Right <= _btExitW + _btMaxW + _btMinW)
                {//최소화 구간에있음
                    if (!FixedSingle)
                    {
                        _isMin = true;
                        _isMax = false;
                        _isExit = false;

                        if (_isClicked) {
                            this.SetPaintRectangle(Width - _btL, 0, _btMinW, _btH);
                            e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 2 * _btH, _btMinW, _btH, GraphicsUnit.Pixel);
                        } else {
                            this.SetPaintRectangle(Width - _btL, 0, _btMinW, _btH);
                            e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 1 * _btH, _btMinW, _btH, GraphicsUnit.Pixel);
                        }

                        //다른버튼 초기화
                        if (Stretch) {
                            if (_isMaxed) {
                                this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 4 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                            } else {
                                this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                                e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 0, _btMaxW, _btH, GraphicsUnit.Pixel);
                            }
                        } else {
                            this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                            e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 3 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                        }
                    }
                    this.SetPaintRectangle(Width - _btL + _btMinW + _btMaxW, 0, _btExitW, _btH);
                    e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW + _btMaxW, 0, _btExitW, _btH, GraphicsUnit.Pixel);
                }
            }
            else
            {
                // 커서가 버튼구간에 있지 않으면 원상복구
                if (!FixedSingle)
                {
                    this.SetPaintRectangle(Width - _btL, 0, _btMinW, _btH);
                    e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 0, _btMinW, _btH, GraphicsUnit.Pixel);
                    if (Stretch) {
                        if (_isMaxed) {
                            this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                            e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 4 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                        } else {
                            this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                            e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 0, _btMaxW, _btH, GraphicsUnit.Pixel);
                        }
                    } else {
                        this.SetPaintRectangle(Width - _btL + _btMinW, 0, _btMaxW, _btH);
                        e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW, 3 * _btH, _btMaxW, _btH, GraphicsUnit.Pixel);
                    }
                }
                this.SetPaintRectangle(Width - _btL + _btMinW + _btMaxW, 0, _btExitW, _btH);
                e.Graphics.DrawImage(pButtons.Image, this.rect, _btMinW + _btMaxW, 0, _btExitW, _btH, GraphicsUnit.Pixel);
            }
            //FixedSingle일때 X버튼 왼쪽부분에 회색선 그리기
            if (FixedSingle) {
                this.SetPaintRectangle(Width - _btL + _btMinW + _btMaxW - 2, 0, 2, _btH);
                e.Graphics.DrawImage(pButtons.Image, this.rect, 0, 0, 2, _btH, GraphicsUnit.Pixel);
            }
            e.Dispose();
            this.forcePaint = false;
            this.repaintButtonOnly = false;
        }

        private void ctlSkin_MouseDown(object sender, MouseEventArgs e)
        {
            _RightX = this.Width - e.X;
            _DownY = this.Height - e.Y;
            if (e.Button == MouseButtons.Left && e.X >= this.windowButtonX && e.Y <= _btH)
            {
                _isClicked = true;
                if (this.rectMin.Contains(e.X, e.Y)) {
                    this.currRect = this.rectMin;
                } else if (this.rectMax.Contains(e.X, e.Y)) {
                    this.currRect = this.rectMax;
                } else {
                    this.currRect = this.rectClose;
                }
                this.forcePaint = true;
                this.Invalidate(false);
            }
            //Resizing
            if (e.Button == MouseButtons.Left && !_isMouseIn && !_isMaxed && Stretch)
            {
                uint SysCommWparam = 0;
                if ((e.X < 8) && (e.Y < 8))
                    SysCommWparam = SC_DRAG_RESIZEUL; // ↖
                else if ((_RightX < 8) && (_DownY < 8))
                    SysCommWparam = SC_DRAG_RESIZEDR; // ↘
                else if ((e.X < 8) && (_DownY < 8))
                    SysCommWparam = SC_DRAG_RESIZEDL; // ↙
                else if ((_RightX < 8) && (e.Y < 8))
                    SysCommWparam = SC_DRAG_RESIZEUR; // ↗
                else if ((e.X < 8))
                    SysCommWparam = SC_DRAG_RESIZEL; // ←
                else if ((_RightX < 8))
                    SysCommWparam = SC_DRAG_RESIZER; // →
                else if ((e.Y < 8))
                    SysCommWparam = SC_DRAG_RESIZEU; // ↑
                else if ((_DownY < 8))
                    SysCommWparam = SC_DRAG_RESIZED; // ↓
                if (SysCommWparam != 0)
                {
                    ReleaseCapture();
                    DefWindowProc(Parent.Handle, WM_SYSCOMMAND, (UIntPtr)SysCommWparam, IntPtr.Zero);
                    this.Cursor = Cursors.Default;
                }
            }
            this.SetPaintRectangle(7, 7, 16, 16);
            this.iconClicked = this.rect.Contains(e.X, e.Y);
        }

        private void ctlSkin_MouseUp(object sender, MouseEventArgs e)
        {
            _isClicked = false;
            this.iconClicked = false;
            if (!_isMouseIn || e.Button != MouseButtons.Left) {
                return;
            }
            Form frm = (this.Parent as Form);
            if (this.currRect.Contains(e.X, e.Y)) {
                if (_isExit) {
                    frm.Close();
                }
                if (_isMin) {
                    SendMessage(this.ParentForm.Handle.ToInt32(), 0x0112, 0xF021, 0);
                    frm.WindowState = FormWindowState.Minimized;
                }
                if (_isMax && Stretch) {
                    if (!_isMaxed) {
                        _isMaxed = true;
                        frm.WindowState = FormWindowState.Maximized;
                    } else {
                        _isMaxed = false;
                        frm.WindowState = FormWindowState.Normal;
                    }
                }
            }
            this.currRect = Rectangle.Empty;
            if (frm == null)
                return;
            this.forcePaint = true;
            this.Invalidate(false);
        }

        private void ctlSkin_MouseLeave(object sender, EventArgs e)
        {
            _isMouseIn = false;
            _isClicked = false;
            this.iconClicked = false;
            this.currRect = Rectangle.Empty;
            if (Screen.GetWorkingArea(this.ParentForm).Contains(this.ParentForm.Bounds)) {
                this.SetPaintRectangle(this.windowButtonX, 0, _windowButtonRelX, _btH);
                this.repaintButtonOnly = true;
                this.Invalidate(this.rect, false);
            } else {
                this.forcePaint = true;
                this.Invalidate(false);
            }
        }

        private void ctlSkin_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.Y <= _TitleBarT)
            {
                this.SetPaintRectangle(7, 7, 16, 16);
                if (this.rect.Contains(e.X, e.Y)) {
                    this.ParentForm.Close();
                    return;
                }
                if (Stretch)
                {
                    Form frm = (this.Parent as Form);
                    if (!_isMaxed)
                    {
                        _isMaxed = true;
                        frm.WindowState = FormWindowState.Maximized;
                    }
                    else
                    {
                        _isMaxed = false;
                        frm.WindowState = FormWindowState.Normal;
                    }
                }
            }
        }

        private void ctlSkin_MouseMove(object sender, MouseEventArgs e)
        {
            _RightX = this.Width - e.X;
            _DownY = this.Height - e.Y;
            if (0 < e.Y && e.Y < _btH && _btRight < _RightX && _RightX < _btL)
            {
                _isMouseIn = true;
                this.Cursor = Cursors.Default;
            }
            else
            {
                _isMouseIn = false;
            }
            this.SetPaintRectangle(this.windowButtonX, 0, _windowButtonRelX, _btH);
            if (_isMouseIn)
            {
                this.repaintButtonOnly = true;
                this.Invalidate(this.rect, false);
                _isReset = true;
            }
            if (_isReset && !_isMouseIn)
            {
                this.repaintButtonOnly = true;
                this.Invalidate(this.rect, false);
                _isReset = false;
            }
            if (!this.iconClicked && !_isClicked && !_isMaxed && !_isMouseIn && e.Y < _TitleBarT && e.X > 8 && (e.X < this.windowButtonX || (e.X < Width - 8 && e.Y > _btH)) && e.Button == MouseButtons.Left)
            {
                DefWindowProc(Parent.Handle, WM_SYSCOMMAND, (UIntPtr)SC_DRAG_MOVE, IntPtr.Zero);
                ReleaseCapture();
            }
            if (!_isMaxed && Stretch && !_isMouseIn)
            {
                if ((e.X < 8) && (e.Y < 8) || (_RightX < 8) && (_DownY < 8))
                    this.Cursor = Cursors.SizeNWSE;
                else if ((_RightX < 8) && (e.Y < 8) || ((e.X < 8) && _DownY < 8))
                    this.Cursor = Cursors.SizeNESW;
                else if ((e.X < 8) || (_RightX < 8))
                    this.Cursor = Cursors.SizeWE;
                else if ((e.Y < 8) || (_DownY < 8))
                    this.Cursor = Cursors.SizeNS;
                else
                    this.Cursor = Cursors.Default;
            }
        }

        private void ctlSkin_Resize(object sender, EventArgs e)
        {
            if ((this.Parent as Form) == null)
                return;
            this.windowButtonX = this.Width - 8 - _windowButtonRelX;
            this.rectMin.X = this.windowButtonX;
            this.rectMax.X = this.rectMin.X + _btMinW;
            this.rectClose.X = this.rectMax.X + _btMaxW;
            this.forcePaint = true;
            this.Invalidate(false);
        }

        private void ParentForm_TextChanged(object sender, EventArgs e)
        {
            this.forcePaint = true;
            this.Invalidate(false);
        }
        #endregion

        #region - Disposer -
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
                this.titleFont.Dispose();
                if (this.icon != null) {
                    this.icon.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion

        #region - Constructor -
        public AppFormSkin()
        {
            this.InitializeComponent();
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.SupportsTransparentBackColor, true);
            this.titleTextY = (int)((_TitleBarT / 2) - (this.titleFont.Height / 2));
        }
        #endregion

        #region - Load -
        private void AppFormSkin_Load(object sender, EventArgs e)
        {
            this.ParentForm.TextChanged += new System.EventHandler(ParentForm_TextChanged);
            this.ParentForm.FormBorderStyle = FormBorderStyle.None;
            this.SendToBack();
            this.Dock = DockStyle.Fill;
            var s = this.ParentForm.MinimumSize;
            if (!this.InDesignMode()) {
                s.Width = this.windowButtonX - 8;
            } else {
                s.Width = this.Width - 8 - _windowButtonRelX - 8;
            }
            s.Height = _TitleBarT + 8;
            this.ParentForm.MinimumSize = s;
        }

        private bool InDesignMode()
        {
            IDesignerHost host;
            if (Site != null) {
                host = Site.GetService(typeof(IDesignerHost)) as IDesignerHost;
                if (host != null) {
                    return host.RootComponent.Site.DesignMode;
                }
            }
            return false;
        }
        #endregion
    }
}
