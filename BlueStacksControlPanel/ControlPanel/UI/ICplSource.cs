﻿using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI
{
    internal interface ICplSource
    {
        string SourceName
        {
            get;
        }

        string SourceVersion
        {
            get;
        }

        bool EnableNotificationMode
        {
            get;
            set;
        }

        bool AllowWindowClose
        {
            get;
            set;
        }

        string DenyWindowCloseReason
        {
            get;
            set;
        }
    }
}
