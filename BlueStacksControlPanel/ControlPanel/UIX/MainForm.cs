﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using BlueStacks.Management.Interop;
using BlueStacks.Management.Utilities;

namespace BlueStacks.ControlPanel.UI
{
    internal partial class MainForm : Form, SingleInstanceApp.IForm
    {
        private static readonly Color cplButtonBGColor = Color.FromArgb(72, 72, 72);

        private Button currentCplButton;

        public MainForm()
        {
            this.InitializeComponent();
            this.Icon = this.trayIcon.Icon = ExeIcons.ControlPanelIcon;
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            this.lblVersion.Text = "version " + version;
            this.Text = this.trayIcon.Text = this.lblName.Text + " v" + version;
            this.trayIcon.ContextMenu = this.trayMenu;
            this.FormLogicConstructor();
        }

        public void HandleNextInstance()
        {
            this.ReactivateWindow();
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Win32Methods.WM_SYSCOMMAND) {
                if ((m.WParam.ToInt32() & 0xFFF0) == Win32Methods.SC_MINIMIZE && this.Visible) {
                    this.Visible = false;
                    this.trayIcon.Visible = true;
                    return;
                }
            }
            base.WndProc(ref m);
        }

        private void ReactivateWindow()
        {
            if (!this.Visible) {
                this.Visible = true;
            }
            this.trayIcon.Visible = false;
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
        
        private void ChangeCplButtonState(Button btn, Color btnColor, Color backColor,
                                          Color foreColor, int borderSize)
        {
            btn.BackColor = btnColor;
            btn.FlatAppearance.MouseOverBackColor = backColor;
            btn.FlatAppearance.BorderColor = backColor;
            btn.FlatAppearance.BorderSize = borderSize;
            btn.ForeColor = foreColor;
        }

        private void trayIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Middle) {
                this.ReactivateWindow();
            }
        }

        private void trayMenuItemShow_Click(object sender, EventArgs e)
        {
            this.ReactivateWindow();
        }

        private void trayMenuItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            this.btnCplLauncher.PerformClick();
        }

        private void cplButtonControl_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn != this.currentCplButton) {
                if (this.currentCplButton != null) {
                    this.ChangeCplButtonState(this.currentCplButton, cplButtonBGColor, Color.Gray, Color.WhiteSmoke, 1);
                }
                this.currentCplButton = btn;
                this.ChangeCplButtonState(btn, Color.DimGray, Color.DimGray, Color.White, 0);
            }
        }
    }
}
