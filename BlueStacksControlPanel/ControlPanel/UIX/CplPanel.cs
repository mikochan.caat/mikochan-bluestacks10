﻿using System.Windows.Forms;

namespace BlueStacks.ControlPanel.UI
{
    internal partial class CplPanel : UserControl
    {
        protected ICplPanelDataSource DataSource
        {
            get;
            private set;
        }

        private CplPanel() : base()
        {
            this.InitializeComponent();
        }

        public CplPanel(ICplPanelDataSource dataSource) : base()
        {
            this.InitializeComponent();
            this.DataSource = dataSource;
        }
    }
}
