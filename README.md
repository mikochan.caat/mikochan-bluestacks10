## BlueStacks Launcher and Control Panel

These set of applications helps keep the "bloatedness" of the BlueStacks App Player (v0.10 below) in check.

You can configure the behavior of BlueStacks-related services, screen/device size, RAM usage, startup behavior using the Control Panel.  
You can use the Launcher to manage the lifetime of the launched BlueStacks process and perform cleanup (clean log files and stop services) upon exit.

## Screenshots

*It's better to show what it actually does...*

![main-screen,launcher-settings](samples/1.png "Main Screen / Launcher Settings")
![device-settings](samples/2.png "Device Settings")
![miscellaneous-settings](samples/3.png "Miscellaneous Settings")
![about-screen](samples/4.png "About Screen")
![shared-folder-settings](samples/5.png "Shared Folder Settings")

## Building

This project was initially built in Visual Studio 2010 for .NET 4.0 but was recently migrated to Visual Studio 2019 for .NET 4.5.2.  
You can build this using Visual Studio 2013 at the very least.

The Control Panel was built using Windows Forms.  
This project does not use any NuGet packages.
